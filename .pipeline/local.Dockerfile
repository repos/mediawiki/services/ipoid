FROM docker-registry.wikimedia.org/nodejs20-slim
USER "root"
ENV HOME="/root"
ENV DEBIAN_FRONTEND="noninteractive"
RUN apt-get update && apt-get install -y "git" "build-essential" "python3-dev" "ca-certificates" "curl" "jq" && rm -rf /var/lib/apt/lists/*
RUN groupadd -o -g "65533" -r "somebody" && useradd -o -m -d "/home/somebody" -r -g "somebody" -u "65533" "somebody" && mkdir -p "/srv/service" && chown "65533":"65533" "/srv/service" && mkdir -p "/opt/lib" && chown "65533":"65533" "/opt/lib"
RUN groupadd -o -g "900" -r "runuser" && useradd -o -m -d "/home/runuser" -r -g "runuser" -u "900" "runuser"
USER "somebody"
ENV HOME="/home/somebody"
WORKDIR "/srv/service"
ENV APP_BASE_PATH="/srv/service" APP_CONFIG_PATH="./config.dev.yaml" DATADIR="/tmp/ipoid" LINK="g++"
COPY --chown=65533:65533 ["package.json", "package-lock.json", "./"]
RUN npm install
COPY --chown=65533:65533 [".", "."]
USER "runuser"
ENV HOME="/home/runuser"
ENV NODE_ENV=""
ENTRYPOINT ["node", "server.js"]
LABEL blubber.variant="development" blubber.version="0.8.0+ba236df"
