// WARNING: Assumes '@@@@@' doesn't occur in the data file

'use strict';

const fs = require( 'fs' );
const ipaddr = require( 'ipaddr.js' );
const os = require( 'os' );
const logger = require( './pipeline-logger.js' ).getPipelineLogger();
const readline = require( 'readline' );
const sqlString = require( 'sqlstring' );
const { getActorTypes, getActorRisks, getTunnels, actorTypes, riskTypes, getActorProxies, getActorBehaviors } = require( './import-data-utils' );
const DATADIR = process.env.DATADIR;

function init( inputFilePath, mode ) {
	return new Promise( ( resolve ) => {
		const stats = {
			changed: 0,
			removed: 0,
			inserted: 0
		};

		function arrayCompare( arr1, arr2 ) {
			if ( arr1 === arr2 ) {
				return true;
			}
			if ( !arr1 || !arr2 ) {
				return false;
			}
			if ( arr1.length !== arr2.length ) {
				return false;
			}
			for ( let i = 0; i < arr1.length; i++ ) {
				if ( arr1[ i ] !== arr2[ i ] ) {
					return false;
				}
			}
			return true;
		}

		function tunnelsCompare( tunnel1, tunnel2 ) {
			if ( tunnel1.operator !== tunnel2.operator ) {
				return false;
			}
			if ( tunnel1.type !== tunnel2.type ) {
				return false;
			}
			return true;
		}

		function tunnelsSort( tunnel1, tunnel2 ) {
			if ( tunnel1.operator < tunnel2.operator ) {
				return -1;
			}
			if ( tunnel1.operator > tunnel2.operator ) {
				return 1;
			}
			return 0;
		}

		function tunnelsArrayCompare( tunnels1, tunnels2 ) {
			const arr1 = getTunnels( tunnels1 );
			const arr2 = getTunnels( tunnels2 );
			if ( arr1 === arr2 ) {
				return true;
			}
			if ( !arr1 || !arr2 ) {
				return false;
			}
			if ( arr1.length !== arr2.length ) {
				return false;
			}
			arr1.sort( tunnelsSort );
			arr2.sort( tunnelsSort );
			for ( let i = 0; i < arr1.length; i++ ) {
				if ( !tunnelsCompare( arr1[ i ], arr2[ i ] ) ) {
					return false;
				}
			}
			return true;
		}

		// Using the IP Context object structure as documented in https://docs.spur.us/#/data-types?id=ip-context-object
		function checkIfChanged( dataPrev, dataCurr ) {
			// IPs should be the same if we got here

			if ( dataPrev.organization !== dataCurr.organization ) {
				return true;
			}
			if ( !!dataPrev.client !== !!dataCurr.client ) {
				return true;
			}
			if ( dataPrev.client ) {
				// client exists on both
				if ( !arrayCompare( dataPrev.client.behaviors, dataCurr.client.behaviors ) ) {
					return true;
				}
				if ( !!dataPrev.client.concentration !== !!dataCurr.client.concentration ) {
					return true;
				}
				if ( dataPrev.client.concentration ) {
					// client.concentration exists on both
					if (
						dataPrev.client.concentration.city !==
						dataCurr.client.concentration.city
					) {
						return true;
					}
					if (
						dataPrev.client.concentration.country !==
						dataCurr.client.concentration.country
					) {
						return true;
					}
					if (
						dataPrev.client.concentration.state !==
						dataCurr.client.concentration.state
					) {
						return true;
					}
				}
				if ( dataPrev.client.count !== dataCurr.client.count ) {
					return true;
				}
				if ( !arrayCompare( dataPrev.client.countries, dataCurr.client.countries ) ) {
					return true;
				}
				if ( !arrayCompare( dataPrev.client.proxies, dataCurr.client.proxies ) ) {
					return true;
				}
				if ( !arrayCompare( dataPrev.client.types, dataCurr.client.types ) ) {
					return true;
				}
			}
			if ( !!dataPrev.location !== !!dataCurr.location ) {
				return true;
			}
			if ( dataPrev.location ) {
				// location exists on both
				if ( dataPrev.location.country !== dataCurr.location.country ) {
					return true;
				}
			}
			if ( !arrayCompare( dataPrev.risks, dataCurr.risks ) ) {
				return true;
			}
			if ( !!dataPrev.tunnels !== !!dataCurr.tunnels ) {
				return true;
			}
			if ( dataPrev.tunnels ) {
				// tunnels exists on both
				if ( !tunnelsArrayCompare( dataPrev.tunnels, dataCurr.tunnels ) ) {
					return true;
				}
			}
			return false;
		}

		function returnAsEncapsulatedString( array ) {
			return array.map( function ( item ) {
				return sqlString.escape( item );
			} ).join( ',' );
		}

		function generateRemoveActorQueries( actor ) {
			let queries = '';
			queries += `DELETE a FROM actor_data_behaviors a INNER JOIN actor_data b on a.actor_data_id = b.pkid WHERE b.ip = '${ actor.ip }';@@@@@`;
			queries += `DELETE a FROM actor_data_proxies a INNER JOIN actor_data b on a.actor_data_id = b.pkid WHERE b.ip = '${ actor.ip }';@@@@@`;
			queries += `DELETE a FROM actor_data_tunnels a INNER JOIN actor_data b on a.actor_data_id = b.pkid WHERE b.ip = '${ actor.ip }';@@@@@`;
			queries += `DELETE FROM actor_data WHERE ip = '${ actor.ip }';@@@@@`;
			return queries;
		}

		function generateInsertActorQueries( actor ) {
			const actorObj = {
				actor_data: {
					ip: actor.ip,
					org: actor.organization,
					client_count: actor.client.count || 0,
					types: actor.client.types ?
						getActorTypes( actor.client.types ) : actorTypes.UNKNOWN,
					conc_city:
						actor.client.concentration && actor.client.concentration.city ? actor.client.concentration.city : '',
					conc_state:
						actor.client.concentration && actor.client.concentration.state ? actor.client.concentration.state : '',
					conc_country:
						actor.client.concentration && actor.client.concentration.country ? actor.client.concentration.country : '',
					countries: actor.client.countries || 0,
					location_country: actor.location.country || '',
					risks: actor.risks ? getActorRisks( actor.risks ) : riskTypes.UNKNOWN
				},
				behaviors: actor.client.behaviors && Array.isArray( actor.client.behaviors ) ?
					getActorBehaviors( actor.client.behaviors ) : [],
				proxies: actor.client.proxies && Array.isArray( actor.client.proxies ) ?
					getActorProxies( actor.client.proxies ) : [],
				tunnels: getTunnels( actor.tunnels )
			};

			let queries = '';
			queries += `INSERT INTO actor_data (${ Object.keys( actorObj.actor_data ).toString() }) VALUES (${ returnAsEncapsulatedString( Object.values( actorObj.actor_data ) ) });@@@@@`;
			actorObj.behaviors.forEach( function ( behavior ) {
				queries += `INSERT INTO actor_data_behaviors (actor_data_id, behavior_id) VALUES( (SELECT pkid FROM actor_data WHERE ip = '${ actorObj.actor_data.ip }'), (SELECT pkid FROM behaviors WHERE behavior = '${ behavior }') );@@@@@`;
			} );
			actorObj.proxies.forEach( function ( proxy ) {
				queries += `INSERT INTO actor_data_proxies (actor_data_id, proxy_id) VALUES( (SELECT pkid FROM actor_data WHERE ip = '${ actorObj.actor_data.ip }'), (SELECT pkid FROM proxies WHERE proxy = '${ proxy }' ) );@@@@@`;
			} );
			if ( actorObj.tunnels ) {
				actorObj.tunnels.forEach( function ( tunnel ) {
					if ( tunnel.anonymous === null || tunnel.anonymous === undefined ) {
						queries += `INSERT INTO actor_data_tunnels (actor_data_id, tunnel_id) VALUES( (SELECT pkid FROM actor_data WHERE ip = '${ actorObj.actor_data.ip }'), (SELECT pkid FROM tunnels WHERE operator = '${ tunnel.operator }' AND anonymous IS NULL ) );@@@@@`;
					} else {
						queries += `INSERT INTO actor_data_tunnels (actor_data_id, tunnel_id) VALUES( (SELECT pkid FROM actor_data WHERE ip = '${ actorObj.actor_data.ip }'), (SELECT pkid FROM tunnels WHERE operator = '${ tunnel.operator }' AND anonymous = ${ tunnel.anonymous } ) );@@@@@`;
					}
				} );
			}
			return queries;
		}

		function generateUpdateActorQueries( oldActor, newActor ) {
			let queries = '';
			queries += generateRemoveActorQueries( oldActor );
			queries += generateInsertActorQueries( newActor );
			return queries;
		}

		/**
		 * Normalize an IP address if present on an actor data object
		 *
		 * @throws Error
		 * @param {Object} data
		 */
		function normalizeIP( data ) {
			if ( !data.ip ) {
				return;
			}
			try {
				data.ip = ipaddr.parse( data.ip ).toNormalizedString();
			} catch ( e ) {
				// IP address is not in a known format
				logger.error(
					{
						err: e  + ': ' + data.ip
					}
				);
				throw e;
			}
		}

		const filePaths = {
			insert: `${ DATADIR }/insert.sql`,
			remove: `${ DATADIR }/remove.sql`,
			update: `${ DATADIR }/update.sql`
		};

		// Remove stale files and recreate them
		Object.keys( filePaths ).forEach( function ( filePath ) {
			fs.rmSync( filePaths[ filePath ], {
				force: true
			} );
			fs.appendFileSync( filePaths[ filePath ], '' );
		} );

		if ( mode !== 'import' ) {
			// In diff mode, add an extra line to compare the final line against.
			// See diff handler documentation for details of the algorithm.
			fs.appendFileSync( inputFilePath, '{}\n' );
		}

		const sortedrl = readline.createInterface( {
			input: fs.createReadStream( inputFilePath ),
			crlfDelay: Infinity,
			highWaterMark: 10000
		} );

		const insertWriteStream = fs.createWriteStream( filePaths.insert, {
			highWaterMark: 10000
		} );
		insertWriteStream.on( 'drain', () => {
			sortedrl.resume();
		} );
		const removeWriteStream = fs.createWriteStream( filePaths.remove, {
			highWaterMark: 10000
		} );
		removeWriteStream.on( 'drain', () => {
			sortedrl.resume();
		} );
		const updateWriteStream = fs.createWriteStream( filePaths.update, {
			highWaterMark: 10000
		} );
		updateWriteStream.on( 'drain', () => {
			sortedrl.resume();
		} );

		let dataPrev = {};
		let justRemoved;
		let justInserted;

		const handlers = {
			// The diff algorithm takes an input file containing JSON data for each IP.
			// Each line of the file is a JSON object representing a single IP address,
			// taken either from the old data file or the new data file. The origin of
			// each line is indicated by `@@@@@<` (old file) or `@@@@@>` (new file) at
			// the end of the line.
			//
			// Lines that are identical between the two files have already been filtered
			// out, so the remaining lines are either only in the old file (IP was
			// removed), only in the new file (IP was added) or in both files (the IP's
			// metadata was changed requiring a database update for that IP, or there was
			// a change that we can ignore).
			//
			// The lines are sorted by IP address, so to determine whether an IP address
			// occurs only in the old file, only in the new file, or in both, we only
			// need to compare consecutive lines.
			//
			// Assumptions:
			// - An IP address occurs maximum twice in the input file.
			// - The lines are sorted by IP address, so lines containing the same IP
			//   address are next to each other.
			//
			// Algorithm:
			// - Compare two consecutive lines (objects) at a time (the first and last
			//   lines are compared to an empty object in addition). Looking only at
			//   the IPs, consider them the current IP and the previous IP.
			// - If the current IP is different from the previous line, and it is from
			//   the OLD file, mark it REMOVED, but don't write the query yet.
			// - If the current IP is different from the previous line, and it is from
			//   the NEW file, mark it INSERTED, but don't write the query yet.
			// - If the previous IP is different from the current line, and it is marked
			//   REMOVED, write the remove query. (It was different from the IPs either
			//   side, so only occurs once.)
			// - If the previous IP is different from the current line, and it is marked
			//   INSERTED, write the insert query. (It was different from the IPs either
			//   side, so only occurs once.)
			// - If the two IPs are the same, check if their objects are different in a
			//   way that requires a database update. If so write the queries. If not,
			//   continue and don't compare either line to any more lines.
			diff: function ( line ) {
				const dataCurr = JSON.parse( line.split( '@@@@@' )[ 0 ] );
				try {
					normalizeIP( dataCurr );
				} catch ( e ) {
					return;
				}
				const currentIsNew = line.endsWith( '>' );
				let writeResult;
				if ( justRemoved && dataCurr.ip === justRemoved ) {
					stats.removed--;
				} else if ( justRemoved && dataCurr.ip !== justRemoved ) {
					writeResult = removeWriteStream.write(
						generateRemoveActorQueries( dataPrev ) + os.EOL
					);
					if ( !writeResult ) {
						// Pause the read stream until writes have a chance to catch up.
						sortedrl.pause();
					}
				}
				if ( justInserted && dataCurr.ip === justInserted ) {
					stats.inserted--;
				} else if ( justInserted && dataCurr.ip !== justInserted ) {
					writeResult = insertWriteStream.write(
						generateInsertActorQueries( dataPrev ) + os.EOL
					);
					if ( !writeResult ) {
						// Pause the read stream until writes have a chance to catch up.
						sortedrl.pause();
					}
				}
				justRemoved = undefined;
				justInserted = undefined;
				if ( dataPrev.ip === dataCurr.ip ) {
					if ( checkIfChanged( dataPrev, dataCurr ) ) {
						stats.changed++;
						writeResult = updateWriteStream.write( generateUpdateActorQueries(
							currentIsNew ? dataPrev : dataCurr,
							currentIsNew ? dataCurr : dataPrev
						) + os.EOL );
						if ( !writeResult ) {
							// Pause the read stream until writes have a chance to catch up.
							sortedrl.pause();
						}
					}
					// No need to compare to the next IP, since it must be different
					// if the IPs each only occurred maximum once in each file
					dataPrev = undefined;
				} else if ( currentIsNew ) {
					// Might be inserted. We'll decrement later if not
					stats.inserted++;
					justInserted = dataCurr.ip;
				} else {
					// Might be removed. We'll decrement later if not
					stats.removed++;
					justRemoved = dataCurr.ip;
				}
				dataPrev = dataCurr;
			},
			import: function ( line ) {
				const data = JSON.parse( line );
				try {
					normalizeIP( data );
				} catch ( e ) {
					return;
				}
				const writeResult = insertWriteStream.write(
					generateInsertActorQueries( data ) + os.EOL
				);
				if ( !writeResult ) {
					// Pause the read stream until writes have a chance to catch up.
					sortedrl.pause();
				}
				stats.inserted++;
			}
		};

		sortedrl.on( 'line', handlers[ mode === 'import' ? 'import' : 'diff' ] );

		sortedrl.on( 'close', () => {
			console.log( stats );
			resolve();
		} );
	} );
}

module.exports = init;
