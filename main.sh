#!/usr/bin/env bash

# Run an initial import of data or update existing data
# Takes the following parameters:
#  --init: true
#  --today: YYYYMMDD
#  --yesterday: YYYYMMDD
#  --debug: true
#
# Use --init to do an initial import. Pass (optional) --today or let the server decide
# Otherwise, pass (optional) --today and --yesterday dates to run diffs on those dates
# and update the database accordingly. --debug stops the script from cleaning up after
# itself, leaving intermediary files.

# Make sure variables exist
debug=""
init=""
updatedb=""
today=""
yesterday=""

# Parse CLI arguments and assign to variables, e.g. --debug true assigns $debug to true
while [ $# -gt 0 ]; do
	if [[ $1 == "--"* ]]; then
		v="${1/--/}"
		declare "$v"="$2"
		shift
	fi
	shift
done

TRACE_ID=$(uuidgen)
export TRACE_ID=$TRACE_ID

# Import structured logger
. logger.sh

set -e

# The data directory may not yet exist, so create it now
mkdir -p "$DATADIR"

DATE_CMD=$(which date)
# on macOS, GNU Date (which supports -d option) is `gdate`
if [ "$(uname)" == "Darwin" ]; then
	DATE_CMD=$(which gdate)
fi

# Number of lines to process in each batch
# prefer --batchsize over BATCH_SIZE over 10000
BATCH_SIZE=${batchsize:-$BATCH_SIZE}
BATCH_SIZE=${BATCH_SIZE:-10000}

# Make sure the database schema is up to date
if [ "$init" = "true" ]; then
	# Reset and reinitialize database
	node -e "require('./init-db.js').init(true);"
else
	# Run database updates, if any
	node -e "require('./init-db.js').init(false);"
fi

# Get run dates
# if --init is true, only today's date is needed for the initial import
if [ "$init" = "true" ]; then
	# If no value for today was passed, set it to today
	today=${today:-$($DATE_CMD '+%Y%m%d')}
# if --updatedb, try to pull values from latest import in db
elif [ "$updatedb" = "true" ]; then
	# Try to get yesterday's date from the database
	yesterday=$(node ./get-latest-run-date.js)

	# FIXME rename "today" to "next" or "current", and "yesterday" to "previous"
	# Set the "today" import value to "yesterday"
	# In practice, this means that our data might be a little out of date compared
	# to if we used current day. However, using current day means we risk
	# fluctuations appearing in the data file, because Spur sometimes updates
	# the current day's file throughout the day, after creating the initial dump.
	# See T356736 for more history.
	today=$($DATE_CMD -d "yesterday" '+%Y%m%d')

	# If "today" is same or older than "yesterday", don't attempt to do anything
	if [[ $today < $yesterday ]]; then
		echo "Value for today $today is older than yesterday $yesterday. Aborting." | json_logger INFO
		exit 0
	fi

	if [ -z "$yesterday" ] || [ -z "$today" ]; then
		echo "Unable to get date from database. Aborting." | json_logger FATAL
		exit 1
	fi
elif [ -n "$yesterday" ] && [ -z "$today" ]; then
	# If only --yesterday is passed, set today's date based on that
	today=$($DATE_CMD -d "$yesterday 1 day" '+%Y%m%d')
elif [ -z "$yesterday" ] && [ -n "$today" ]; then
	# If only --today is passed, set yesterday's date based on that
	yesterday=$($DATE_CMD -d "$today 1 day ago" '+%Y%m%d')
elif [ -z "$yesterday" ] && [ -z "$today" ]; then
	# If neither are passed, set today's date and yesterday's date from that
	today=$($DATE_CMD '+%Y%m%d')
	yesterday=$($DATE_CMD -d "1 day ago" '+%Y%m%d')
fi
# If none of the conditionals were met, both today and yesterday values were passed so use them as-is

# if --init is true, then run initial import
if [ "$init" = "true" ]; then
	node -e "require('./get-feed.js').init('$today');"
	./diff.sh --today "$DATADIR"/"$today".json.gz --debug "$debug"
	./import.sh --debug "$debug" --batchsize "$BATCH_SIZE"
	exit
fi

# Get the state of the import in the database
import_status=$(node -e "require('./import-status-utils.js').getImportStatus('$yesterday', '$today');")

# This script is expected to run a few times a day so check if the import is already done and abort if so
if [[ $(echo "$import_status" | jq -r '.status') == 'COMPLETE' ]]; then
	echo "Import with these dates already run. Aborting." | json_logger DEBUG
	exit
fi

# Check for errors and incomplete imports before attempting an update import
if [[ $(echo "$import_status" | jq -r '.status') == 'ERROR' ]]; then
	# If any batches failed, try to rerun them by passing along an --includelist of batches to retry.
	# Additional imports shouldn't be allowed to run until all failed batches have been resolved.
	today=$(echo "$import_status" | jq -r '.today')
	yesterday=$(echo "$import_status" | jq -r '.yesterday')
	echo "Found failed batches from a previous run; attempting to re-import batches from an update from $yesterday to $today" | json_logger DEBUG
elif [[ $(echo "$import_status" | jq -r '.status') == 'INCOMPLETE' ]]; then
	# Check if there are any incomplete imports and try to finish them by passing along an --excludelist of already finished batches
	today=$(echo "$import_status" | jq -r '.today')
	yesterday=$(echo "$import_status" | jq -r '.yesterday')
	echo "Found incomplete import. Attempting to restart import from $yesterday and $today." | json_logger DEBUG
fi

# Import can continue as normal
if [[ $(echo "$import_status" | jq -r '.status') == 'NOT_STARTED' ]]; then
	# If values were not passed by the user, $yesterday comes from the database and $today from the server.
	# Since the script is expected to run multiple times a day, it's possible that $yesterday and $today
	# end up identical, as they're obtained independently of one another.
	if [[ $yesterday == "$today" ]]; then
		echo "Dates for yesterday ($yesterday) and today ($today) are identical. Aborting." | json_logger DEBUG
		exit 0
	fi

	echo "Starting normal import." | json_logger DEBUG
fi

node -e "require('./get-feed.js').init('$today');"
node -e "require('./get-feed.js').init('$yesterday');"
./diff.sh --today "$DATADIR"/"$today".json.gz --yesterday "$DATADIR"/"$yesterday".json.gz --debug "$debug"
# TODO: Fix the shellcheck issues
# shellcheck disable=SC2068
# shellcheck disable=SC2116
./import.sh --debug "$debug" --batchsize "$BATCH_SIZE" --today "$DATADIR"/"$today".json.gz --yesterday "$DATADIR"/"$yesterday".json.gz --importstatus "$(echo "$import_status" | jq -r '.status')"
