#!/usr/bin/env bash
# Structured logging for Logstash
#
# Adapted from https://sevic.dev/notes/bash-script-json-logging/, fields match
# the format specified in Elastic Common Schema (ECS)
# @see https://www.elastic.co/guide/en/ecs/8.10/ecs-log.html
#
# Example usage:
# - echo "foo" | json_logger INFO
# - json_logger ERROR < somefile.txt

DATE_CMD=$(which date)
# on macOS, GNU Date (which supports -d option) is `gdate`
if [ "$(uname)" == "Darwin" ]; then
	DATE_CMD=$(which gdate)
fi

declare -A log_levels=([FATAL]=fatal [ERROR]=error [WARNING]=warning [INFO]=info [DEBUG]=debug)

json_logger() {
	log_level=$1
	level=${log_levels[$log_level]}
	timestamp=$($DATE_CMD -u +"%Y-%m-%dT%H:%M:%S.%3NZ")
	jq -M --raw-input --compact-output \
		'{
      "log.level": "'"$level"'",
      "@timestamp": "'"$timestamp"'",
      "process.pid": '$$',
      "host.hostname": "'"$HOSTNAME"'",
      "ecs.version": "8.10.0",
      "message": .,
      "trace.id": "'"$TRACE_ID"'"
    }'
}
