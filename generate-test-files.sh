#!/usr/bin/env bash

set -e

gzip -c ./test/data/20000101_fake.json > "$DATADIR"/20000101_fake.json.gz
gzip -c ./test/data/20000102_fake.json > "$DATADIR"/20000102_fake.json.gz

# Generate files for testing import
node -e "require('./init-db.js').init(true);" && ./main.sh --init true --today 20000101_fake --debug true
mv "$DATADIR"/insert.sql ./test/data/import.statements.sql

# Generate files for testing diff
./main.sh --today 20000102_fake --yesterday 20000101_fake --debug true
mv "$DATADIR"/properties.json ./test/data/20000102_fake.properties.json
mv "$DATADIR"/yesterday_today.unique.sorted ./test/data/20000101_20000102_fake.unique.sorted.json
mv "$DATADIR"/statements.sql ./test/data/diff.statements.sql
