'use strict';

const mariadb = require( 'mariadb' );
const DATADIR = process.env.DATADIR;
const logger = require( './pipeline-logger.js' ).getPipelineLogger();

const jobStates = {
	STARTED: 0,
	COMPLETE: 1,
	ERROR: 2,
	RETRIED: 3
};

const options = {
	host: process.env.MYSQL_HOST,
	user: process.env.MYSQL_RW_USER,
	password: process.env.MYSQL_RW_PASS,
	database: process.env.MYSQL_DATABASE,
	port: process.env.MYSQL_PORT
};

/**
 * Get the information of any batches that have failed and output it as
 * an object containing the batch recreation information (batch file, feed inputs).
 *
 * @return {Array}
 */
async function getFailedBatches() {
	const connection = await mariadb.createConnection( options );
	let failedBatches = await connection.query( `
		SELECT
			feed_file_yesterday,
			feed_file_today,
			batch_file
		FROM
			import_status
		WHERE
			batch_status = ?;
	`, [ jobStates.ERROR ] );

	await connection.end();

	failedBatches = convertBufferToString( failedBatches );
	return failedBatches;
}

/**
 * Check if any imports are incomplete and return a list of untried batches
 * if any are found.
 * Two kinds of incomplete import can be found:
 * 1. Today's import is incomplete:
 *      This will check the import status from today's and yesterday's feed dates
 * 2. Today's import never completed the first batch or errored out:
 *      This will check the import status of the import before today
 *      as if it never completed a batch then a fresh import can be started
 *      and if it caught on an error, getFailedBatch() would catch it instead
 *
 * @return {Object}
 */
async function getIncompleteImport() {
	const connection = await mariadb.createConnection( options );

	// Get the date of the last run import with at least one successful batch
	// If an import started and errored out on the first batch, it would
	// be caught by another check and then otherwise, the import has not been
	// attempted
	let latestImportDates = await connection.query( `
		SELECT
			batch_count,
			feed_file_yesterday,
			feed_file_today
		FROM
			import_status
		WHERE
			batch_status = ?
		ORDER BY timestamp
		DESC
		LIMIT 1;
	`, [ jobStates.COMPLETE ] );
	latestImportDates = convertBufferToString( latestImportDates );
	latestImportDates = latestImportDates[ 0 ];

	// Use the date to count how many batches of the import have been completed
	let latestImportBatches = await connection.query( `
		SELECT
			DISTINCT batch_file
		FROM
			import_status
		WHERE
			feed_file_yesterday = ?
		AND
			feed_file_today = ?
		AND
			batch_status = ?;
	`, [
		latestImportDates.feed_file_yesterday ? `${ DATADIR }/${ latestImportDates.feed_file_yesterday }.json.gz` : '',
		latestImportDates.feed_file_today ? `${ DATADIR }/${ latestImportDates.feed_file_today }.json.gz` : '',
		jobStates.COMPLETE
	] );
	latestImportBatches = convertBufferToString( latestImportBatches );

	await connection.end();

	// Return the status of the latest import
	// If the import ran but is incomplete, pass along a list of
	// completed batches so a continued import can skip them
	return {
		yesterday: latestImportDates.feed_file_yesterday,
		today: latestImportDates.feed_file_today,
		completedBatches: latestImportBatches.length === latestImportDates.batch_count ?
			null : latestImportBatches
	};
}

/**
 * Print the list of failed batches, which should be retried, or completed batches that form part of an incomplete import,
 * depending on the status of the last import.
 * For use by import.sh.
 *
 * @param {string} importStatus The status of the last import.
 * @return {Promise<void>}
 */
async function getBatches( importStatus ) {
	if ( importStatus === 'ERROR' ) {
		const failedBatches = await getFailedBatches();

		console.log( JSON.stringify( {
			include: failedBatches.map( ( batch ) => batch.batch_file ),
			exclude: []
		} ) );
		return;
	}

	const incompleteImports = await getIncompleteImport();
	const completedBatches = incompleteImports.completedBatches || [];

	console.log( JSON.stringify( {
		include: [],
		exclude: completedBatches.map( ( batch ) => batch.batch_file )
	} ) );
}

/**
 * Return the status of an import (defined by a yesterday and today feed)
 * The following states are possible:
 *   - if failed batches are found: return ERROR and the array of batches to retry
 *   - if an incomplete import is found: return INCOMPLETE and the array of batches to skip
 *   - if the import is complete: return COMPLETE
 *   - if the import hasn't been attempted: return NOT_STARTED
 * main.sh will use this status to determine what next step to perform,
 * if any, and is expected to only ever pick one step per run.
 *
 * @param {string} yesterday - file name of yesterday's feed
 * @param {string} today -  file name of today's feed
 *
 * @return {Promise<void>}
 */
async function getImportStatus( yesterday, today ) {
	const params = {
		status: null,
		yesterday: null,
		today: null,
		retryBatches: null,
		completedBatches: null
	};

	// Check for failed imports
	const failedBatches = await getFailedBatches();

	// If there are any failed imports, return the batches and dates
	// that failed so they can be retried
	//
	// Because imports run serially and only after the previous one has been
	// completed successfully, the yesterday and today dates for all
	// batches returned are expected to be the same
	if ( failedBatches.length ) {
		console.log( JSON.stringify( { ...params, ...{
			status: 'ERROR',
			yesterday: failedBatches[ 0 ].feed_file_yesterday,
			today: failedBatches[ 0 ].feed_file_today,
			retryBatches: failedBatches.map( function ( batch ) {
				return batch.batch_file;
			} )
		} } ) );
		return;
	}

	// Check for incomplete imports
	const latestKnownImportStatus = await getIncompleteImport();

	// If an incomplete import is found, return the dates
	// and the batches that have been completed so they can be skipped
	if ( latestKnownImportStatus.completedBatches ) {
		console.log( JSON.stringify( { ...params, ...{
			status: 'INCOMPLETE',
			yesterday: latestKnownImportStatus.yesterday,
			today: latestKnownImportStatus.today,
			completedBatches: latestKnownImportStatus.completedBatches.map( function ( batch ) {
				return batch.batch_file;
			} )
		} } ) );
		return;
	}

	// Otherwise, the import status check returned a complete latest import
	// Compare the dates returned with the dates passed in to see if the import
	// was already run and completed and return if so
	if (
		yesterday === latestKnownImportStatus.yesterday &&
		today === latestKnownImportStatus.today
	) {
		console.log( JSON.stringify( { ...params, ...{
			status: 'COMPLETE',
			yesterday: yesterday,
			today: today
		} } ) );
		return;
	}

	// All other cases exhausted; this is an import that hasn't been run yet
	console.log( JSON.stringify( { ...params, ...{
		status: 'NOT_STARTED',
		yesterday: yesterday,
		today: today
	} } ) );
	return;
}

/**
 * Update the batch status when it's been retried
 *
 * @param {string} batchFile - filepath to the batch file that's been retried
 * @param {string} yesterday - filepath to the batch's yesterday feed
 * @param {string} today - filepath to batch's today feed
 * @param {string} originalType - type of batch that import corrected (STARTED, ERROR)
 */
async function updateBatchStatus( batchFile, yesterday, today, originalType ) {
	// Mark the processed errored-out batch files as retried
	const connection = await mariadb.createConnection( options );
	await connection.query( `
	UPDATE import_status
		SET
			batch_status = ?
		WHERE
			feed_file_yesterday = ? AND
			feed_file_today = ? AND
			batch_status = ? AND
			batch_file = ?;
	`, [ jobStates.RETRIED, yesterday, today, jobStates[ originalType ], batchFile ] );
	logger.info( `Batch file ${ batchFile } was restarted.` );
	await connection.end();
}

/**
 * Batch and feed files come back as filepaths in a buffer
 * Transform every buffer into a string and the feed filepaths into file names
 * Commands later expect the filename, not a path
 *
 * @param {string} arr - Array with objects representing batches that have been run
 *
 * @return {Array}
 */
function convertBufferToString( arr ) {
	arr.map( function ( batch ) {
		const dateGroup = /([1-2][0-9][0-9][0-9])(0[1-9]|1[0-2])(0[1-9]|[1-2][0-9]|3[0-1]).*(?=.json.gz)/g;
		if ( batch.feed_file_yesterday ) {
			batch.feed_file_yesterday = batch.feed_file_yesterday.toString().match( dateGroup );
			batch.feed_file_yesterday = batch.feed_file_yesterday ?
				batch.feed_file_yesterday[ 0 ] : null;
		}
		if ( batch.feed_file_today ) {
			batch.feed_file_today = batch.feed_file_today.toString().match( dateGroup );
			batch.feed_file_today = batch.feed_file_today ?
				batch.feed_file_today[ 0 ] : null;
		}
		if ( batch.batch_file ) {
			batch.batch_file = batch.batch_file.toString();
		}
		return batch;
	} );
	return arr;
}

module.exports.jobStates = jobStates;
module.exports.getImportStatus = getImportStatus;
module.exports.updateBatchStatus = updateBatchStatus;
module.exports.getBatches = getBatches;
