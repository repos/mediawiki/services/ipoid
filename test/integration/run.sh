#!/usr/bin/env bash

set -e

# Clean test files, if they exist
rm -rf "$DATADIR"
mkdir -p "$DATADIR"

# Make a compressed copy of JSON data, since that is what import script expects
gzip -c test/data/20000101_fake.json > "$DATADIR"/20000101_fake.json.gz
gzip -c test/data/20000102_fake.json > "$DATADIR"/20000102_fake.json.gz

# N.B. The call to init-db, the first main.sh invocation, and the last main.sh invocation
# will have a separate TRACE_ID values
# that is expected, because these are all considered separate
# pipeline runs

# Setup the DB
TRACE_ID=$(uuidgen)
export TRACE_ID=$TRACE_ID
node -e "require('./init-db.js').init(true);"

# Initial import
TRACE_ID=$(uuidgen)
export TRACE_ID=$TRACE_ID
./main.sh --init true --today 20000101_fake --debug true

# Update
TRACE_ID=$(uuidgen)
export TRACE_ID=$TRACE_ID
./main.sh --today 20000102_fake --yesterday 20000101_fake --debug true

# Check that API returns a 200
[ $(curl -s localhost:6927/feed/v1/ip/1.2.3.4 -o /dev/stderr -w "%{http_code}") -eq 200 ]

[ $(curl -s localhost:6927/feed/v1/ip/1.2.3.4?usePrefixSearch=1 | jq '. | length') -eq 2 ]

# IPv6 formatted differently from the data
[ $(curl -s localhost:6927/feed/v1/ip/2a02:fEC0:0:0:0:0:0:0 -o /dev/stderr -w "%{http_code}") -eq 200 ]
