'use strict';

const DATADIR = process.env.DATADIR;
const updateDb = require( '../../../update-db.js' );
const importProperties = require( '../../../import-properties.js' );
const initDb = require( '../../../init-db.js' );
const { assert } = require( 'chai' );
const mariadb = require( 'mariadb' );
const fs = require( 'fs' );

const options = {
	host: process.env.MYSQL_HOST,
	user: process.env.MYSQL_RW_USER,
	password: process.env.MYSQL_RW_PASS,
	database: process.env.MYSQL_DATABASE,
	port: process.env.MYSQL_PORT
};

const expected = {
	actorData: [
		{
			ip: '1.2.3.4',
			org: 'Organization 1',
			clientCount: 0,
			types: 1,
			concCity: '',
			concState: '',
			concCountry: '',
			countries: 0,
			locationCountry: 'VN',
			risks: 0
		},
		{
			ip: '1.2.3.40',
			org: 'Organization 1',
			clientCount: 0,
			types: 1,
			concCity: '',
			concState: '',
			concCountry: '',
			countries: 0,
			locationCountry: 'VN',
			risks: 0
		},
		{
			ip: '5.6.7.8',
			org: 'Organization 1',
			clientCount: 0,
			types: 1,
			concCity: '',
			concState: '',
			concCountry: '',
			countries: 0,
			locationCountry: 'VN',
			risks: 0
		},
		{
			ip: '9.10.11.12',
			org: 'Organization 1',
			clientCount: 1,
			types: 1,
			concCity: '',
			concState: '',
			concCountry: '',
			countries: 0,
			locationCountry: 'VN',
			risks: 0
		},
		{
			ip: '13.14.15.16',
			org: 'Organization 1',
			clientCount: 1,
			types: 2,
			concCity: 'Quận Bảy',
			concState: 'Ho Chi Minh',
			concCountry: 'VN',
			countries: 1,
			locationCountry: 'VN',
			risks: 0
		},
		{
			ip: '17.18.19.20',
			org: 'Organization 1',
			clientCount: 1,
			types: 1,
			concCity: 'Cầu Giấy',
			concState: 'Hanoi',
			concCountry: 'VN',
			countries: 1,
			locationCountry: 'VN',
			risks: 0
		},
		{
			ip: '81.82.83.84',
			clientCount: 0,
			concCity: '',
			concCountry: '',
			concState: '',
			countries: 0,
			locationCountry: 'US',
			org: 'Organization 5',
			risks: 1,
			types: 1
		},
		{
			clientCount: 0,
			concCity: '',
			concCountry: '',
			concState: '',
			countries: 0,
			ip: '85.86.87.88',
			locationCountry: 'US',
			org: 'Organization 5',
			risks: 1,
			types: 1
		},
		{
			ip: '41.42.43.44',
			org: 'Organization 2',
			clientCount: 0,
			types: 1,
			concCity: '',
			concState: '',
			concCountry: '',
			countries: 0,
			locationCountry: 'PK',
			risks: 0
		},
		{
			ip: '45.46.47.48',
			org: 'Organization 3',
			clientCount: 1,
			types: 16,
			concCity: '',
			concState: '',
			concCountry: '',
			countries: 0,
			locationCountry: 'PK',
			risks: 0
		},
		{
			ip: '49.50.51.52',
			org: 'Organization 3',
			clientCount: 0,
			types: 1,
			concCity: '',
			concState: '',
			concCountry: '',
			countries: 0,
			locationCountry: 'PK',
			risks: 0
		},
		{
			ip: '53.54.55.56',
			org: 'Organization 4',
			clientCount: 0,
			types: 1,
			concCity: '',
			concState: '',
			concCountry: '',
			countries: 0,
			locationCountry: 'PK',
			risks: 0
		},
		{
			ip: '57.58.59.60',
			org: 'Organization 3',
			clientCount: 0,
			types: 1,
			concCity: '',
			concState: '',
			concCountry: '',
			countries: 0,
			locationCountry: 'PK',
			risks: 0
		},
		{
			ip: '61.62.63.64',
			org: 'Organization 5',
			clientCount: 0,
			types: 1,
			concCity: '',
			concState: '',
			concCountry: '',
			countries: 0,
			locationCountry: 'US',
			risks: 1
		},
		{
			ip: '65.66.67.68',
			org: 'Organization 5',
			clientCount: 0,
			types: 1,
			concCity: '',
			concState: '',
			concCountry: '',
			countries: 0,
			locationCountry: 'US',
			risks: 1
		},
		{
			ip: '69.70.71.72',
			org: 'Organization 5',
			clientCount: 0,
			types: 1,
			concCity: '',
			concState: '',
			concCountry: '',
			countries: 0,
			locationCountry: 'US',
			risks: 1
		},
		{
			ip: '73.74.75.76',
			org: 'Organization 5',
			clientCount: 0,
			types: 1,
			concCity: '',
			concState: '',
			concCountry: '',
			countries: 0,
			locationCountry: 'US',
			risks: 1
		},
		{
			ip: '77.78.79.80',
			org: 'Organization 5',
			clientCount: 0,
			types: 1,
			concCity: '',
			concState: '',
			concCountry: '',
			countries: 0,
			locationCountry: 'US',
			risks: 1
		},
		{
			ip: '89.90.91.92',
			org: 'Organization 1',
			clientCount: 0,
			types: 1,
			concCity: '',
			concState: '',
			concCountry: '',
			countries: 0,
			locationCountry: 'US',
			risks: 1
		},
		{
			clientCount: 0,
			concCity: '',
			concCountry: '',
			concState: '',
			countries: 0,
			ip: '93.94.95.96',
			locationCountry: 'US',
			org: 'Organization 5',
			risks: 1,
			types: 1
		},
		{
			clientCount: 0,
			concCity: '',
			concCountry: '',
			concState: '',
			countries: 0,
			ip: '2a02:fec0:0:0:0:0:0:0',
			locationCountry: '',
			org: '0',
			risks: 2,
			types: 1
		}
	],
	actorDataBehaviors: 2,
	actorDataProxies: 17,
	actorDataTunnels: 7,
	importStatus: [
		{
			yesterday: '/tmp/20000101_fake.json.gz',
			today: '/tmp/20000102_fake.json.gz',
			count: 1,
			status: 1
		}
	]
};

function byIp( a, b ) {
	if ( a.ip < b.ip ) {
		return -1;
	}
	if ( a.ip > b.ip ) {
		return 1;
	}
	return 0;
}

describe( 'update-db', () => {
	before( async function () {
		fs.writeFileSync(
			`${ DATADIR }/job.info`,
			'feed_file_yesterday:/tmp/20000101_fake.json.gz\n' +
			'feed_file_today:/tmp/20000102_fake.json.gz\n' +
			'batch_count:1'
		);
		await initDb.init( true );
		await importProperties( './test/data/20000102_fake.properties.json', 'true' );
		await updateDb( 'test/data/diff.statements.sql' );
	} );

	after( async function () {
		await initDb.init( true );
	} );

	it( 'should update the actor_data table correctly', async () => {
		const connection = await mariadb.createConnection( options );
		const actorData = await connection.query( 'SELECT * FROM actor_data;' );
		const actual = actorData.map( ( row ) => {
			return {
				ip: row.ip.toString(),
				org: row.org.toString(),
				clientCount: row.client_count,
				types: row.types,
				concCity: row.conc_city.toString(),
				concState: row.conc_state.toString(),
				concCountry: row.conc_country.toString(),
				countries: row.countries,
				locationCountry: row.location_country.toString(),
				risks: row.risks
			};
		} );
		assert.deepEqual( actual.sort( byIp ), expected.actorData.sort( byIp ) );
		await connection.end();
	} );

	it( 'should update the actor_data_behaviors table correctly', async () => {
		const connection = await mariadb.createConnection( options );
		const actual = await connection.query( 'SELECT * FROM actor_data_behaviors;' );
		assert.equal( actual.length, expected.actorDataBehaviors );
		await connection.end();
	} );

	it( 'should update the actor_data_proxies table correctly', async () => {
		const connection = await mariadb.createConnection( options );
		const actual = await connection.query( 'SELECT * FROM actor_data_proxies;' );
		assert.equal( actual.length, expected.actorDataProxies );
		await connection.end();
	} );

	it( 'should update the actor_data_tunnels table correctly', async () => {
		const connection = await mariadb.createConnection( options );
		const actual = await connection.query( 'SELECT * FROM actor_data_tunnels;' );
		assert.equal( actual.length, expected.actorDataTunnels );
		await connection.end();
	} );

	it( 'should update the import_status table correctly', async () => {
		const connection = await mariadb.createConnection( options );
		const importStatus = await connection.query( 'SELECT * FROM import_status;' );
		const actual = importStatus.map( ( row ) => {
			return {
				yesterday: row.feed_file_yesterday.toString(),
				today: row.feed_file_today.toString(),
				count: row.batch_count,
				status: row.batch_status
			};
		} );
		assert.deepEqual( actual, expected.importStatus );
		await connection.end();
	} );

	it( 'should update the import_status table correctly with errors', async () => {
		// This will cause duplicate entry errors
		await updateDb( 'test/data/diff.statements.sql' );
		const connection = await mariadb.createConnection( options );
		const importStatus = await connection.query( 'SELECT * FROM import_status;' );
		const actual = importStatus.map( ( row ) => {
			return {
				yesterday: row.feed_file_yesterday.toString(),
				today: row.feed_file_today.toString(),
				count: row.batch_count,
				status: row.batch_status
			};
		} );
		assert.deepEqual( actual, [
			expected.importStatus[ 0 ],
			{
				yesterday: '/tmp/20000101_fake.json.gz',
				today: '/tmp/20000102_fake.json.gz',
				count: 1,
				status: 1
			}
		] );
		await connection.end();
	} );

	it( 'should update the import_status table correctly with multiple errors', async () => {
		// Override for the duration of this script
		process.env.BATCH_SIZE = 5;
		// This will cause duplicate entry errors
		await updateDb( 'test/data/diff.statements.sql' );
		const connection = await mariadb.createConnection( options );
		const importStatus = await connection.query( 'SELECT * FROM import_status;' );
		const actual = importStatus.map( ( row ) => {
			return {
				yesterday: row.feed_file_yesterday.toString(),
				today: row.feed_file_today.toString(),
				count: row.batch_count,
				status: row.batch_status
			};
		} );
		assert.deepEqual( actual, [
			expected.importStatus[ 0 ],
			{
				yesterday: '/tmp/20000101_fake.json.gz',
				today: '/tmp/20000102_fake.json.gz',
				count: 1,
				status: 1
			},
			{
				yesterday: '/tmp/20000101_fake.json.gz',
				today: '/tmp/20000102_fake.json.gz',
				count: 1,
				status: 1
			}
		] );
		await connection.end();
	} );
} );
