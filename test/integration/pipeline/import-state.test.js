'use strict';

const DATADIR = process.env.DATADIR;
const { assert } = require( 'chai' );
const { jobStates } = require( '../../../import-status-utils' );
const { promisify } = require( 'util' );
const childProcessExec = promisify( require( 'child_process' ).exec );
const initDb = require( '../../../init-db.js' );
const mariadb = require( 'mariadb' );

const options = {
	host: process.env.MYSQL_HOST,
	user: process.env.MYSQL_RW_USER,
	password: process.env.MYSQL_RW_PASS,
	database: process.env.MYSQL_DATABASE,
	port: process.env.MYSQL_PORT
};

/**
 * Helper function to execute a subprocess and print the contents of its stdout upon completion.
 *
 * @param {string} process The process to execute.
 * @return {Promise<void>}
 */
async function exec( process ) {
	const { stdout } = await childProcessExec( process );
	console.log( stdout.trim() );
}

describe( 'import-state', () => {
	// Load the data
	before( async function () {
		const files = [
			'19990101_fake.json',
			'19990102_fake.json',
			'19990102_fake_error.json',
			'19990103_fake.json',
			'19990201.json',
			'19990202.json',
			'19990203.json'
		];
		await Promise.all(
			files.map( async ( file ) => {
				await exec( `cp ./test/data/${ file } ${ DATADIR }` );
				await exec( `gzip ${ DATADIR }/${ file } -f` );
			} )
		);
	} );

	// Reset between tests
	beforeEach( async function () {
		// Initial import. Import scripts assume that the initial import was successful and
		// only manage errors in updates.
		await exec( './main.sh --init true --today 19990101_fake --debug true --batchsize 1' );
	} );

	// Tear down database after test
	after( async function () {
		await initDb.init( true );
	} );

	async function ipCount( ip ) {
		const connection = await mariadb.createConnection( options );

		const ipExistsQuery = await connection.query( `
			SELECT COUNT(*)
			FROM actor_data
			WHERE ip = ?
		`, [ ip ] );

		await connection.end();

		return Number( ipExistsQuery[ 0 ][ 'COUNT(*)' ] );
	}

	it( 'should be able to run successive successful imports', async () => {
		const connection = await mariadb.createConnection( options );

		// Initial import and assert it completed
		await exec( './main.sh --init true --today 19990201 --debug true --batchsize 1' );
		let initialImportBatchCount = await connection.query( `
			SELECT COUNT(*)
			FROM import_status
			WHERE feed_file_yesterday = ?
			AND feed_file_today = ?
			AND batch_status = ?
		`, [ '', `${ DATADIR }/19990201.json.gz`, jobStates.COMPLETE ] );
		initialImportBatchCount = Number( initialImportBatchCount[ 0 ][ 'COUNT(*)' ] );
		assert.equal( initialImportBatchCount, 3 );

		// Run an update
		await exec( './main.sh --today 19990202 --yesterday 19990201 --debug true --batchsize 1' );
		let firstUpdateBatchCount = await connection.query( `
			SELECT COUNT(*)
			FROM import_status
			WHERE feed_file_yesterday = ?
			AND feed_file_today = ?
			AND batch_status = ?
		`, [ `${ DATADIR }/19990201.json.gz`, `${ DATADIR }/19990202.json.gz`, jobStates.COMPLETE ] );
		firstUpdateBatchCount = Number( firstUpdateBatchCount[ 0 ][ 'COUNT(*)' ] );
		assert.equal( firstUpdateBatchCount, 6 );

		// Run another update
		await exec( './main.sh --today 19990203 --yesterday 19990202 --debug true --batchsize 1' );
		let secondUpdateBatchCount = await connection.query( `
			SELECT COUNT(*)
			FROM import_status
			WHERE feed_file_yesterday = ?
			AND feed_file_today = ?
			AND batch_status = ?
		`, [ `${ DATADIR }/19990202.json.gz`, `${ DATADIR }/19990203.json.gz`, jobStates.COMPLETE ] );
		secondUpdateBatchCount = Number( secondUpdateBatchCount[ 0 ][ 'COUNT(*)' ] );
		assert.equal( secondUpdateBatchCount, 6 );

		connection.end();
	} );

	it( 'shouldn\'t try to import if dates are identical', async () => {
		const connection = await mariadb.createConnection( options );

		// Run an update
		await exec( './main.sh --today 19990102_fake --yesterday 19990101_fake --debug true --batchsize 1' );
		let importStatusResult = await connection.query( 'SELECT COUNT(*) FROM import_status' );
		importStatusResult = Number( importStatusResult[ 0 ][ 'COUNT(*)' ] );
		// Expect 3 batches from initial import and 6 from update
		assert.equal( importStatusResult, 9 );

		// Run an update with duplicate dates, which shouldn't do anything
		await exec( './main.sh --today 19990102_fake --yesterday 19990102_fake --debug true --batchsize 1' );
		let finalImportStatusResult = await connection.query( 'SELECT COUNT(*) FROM import_status' );
		finalImportStatusResult = Number( finalImportStatusResult[ 0 ][ 'COUNT(*)' ] );
		// Expect same 9 batches
		assert.equal( finalImportStatusResult, importStatusResult );

		await connection.end();
	} );

	it( 'shouldn\'t re-run already successful imports', async () => {
		const connection = await mariadb.createConnection( options );

		// Run an update
		await exec( './main.sh --today 19990102_fake --yesterday 19990101_fake --debug true --batchsize 1' );

		// Re-run the update which shouldn't do anything
		await exec( './main.sh --today 19990102_fake --yesterday 19990101_fake --debug true --batchsize 1' );

		let importStatusResult = await connection.query( 'SELECT COUNT(*) FROM import_status' );
		importStatusResult = Number( importStatusResult[ 0 ][ 'COUNT(*)' ] );
		// Expect 3 batches from initial import and 6 from update, 0 from re-run
		assert.equal( importStatusResult, 9 );

		// Confirm that only 6 batches exist for the update (which was run once and aborted the second time)
		let updateBatchCountResult = await connection.query( `
			SELECT batch_count
			FROM import_status
			WHERE feed_file_yesterday = ?
			AND feed_file_today = ?
			LIMIT 1;
	`, [ `${ DATADIR }/19990101_fake.json.gz`, `${ DATADIR }/19990102_fake.json.gz` ] );
		updateBatchCountResult = Number( updateBatchCountResult[ 0 ].batch_count );
		assert.equal( updateBatchCountResult, 6 );

		connection.end();
	} );

	it( 'should attempt to continue incomplete imports', async () => {
		const connection = await mariadb.createConnection( options );

		// Import with an simulated incomplete state
		await exec( './main.sh --today 19990102_fake --yesterday 19990101_fake --debug true --batchsize 1' );
		await connection.query( `
			DELETE FROM import_status
			WHERE feed_file_yesterday = ?
			AND feed_file_today = ?
			AND batch_file = ?;
		`, [ `${ DATADIR }/19990101_fake.json.gz`, `${ DATADIR }/19990102_fake.json.gz`, `${ DATADIR }/sub/query_split_aaaaf.sql` ] );
		await connection.query( `
			DELETE from actor_data
			WHERE ip = ?
		`, [ '99.99.99.99' ] );

		// Assert that the db state reflects manually deleted batch (only 5/6 batches recorded)
		let incompleteImportBatchCount = await connection.query( `
			SELECT COUNT(*)
			FROM import_status
			WHERE feed_file_yesterday = ?
			AND feed_file_today = ?
		`, [ `${ DATADIR }/19990101_fake.json.gz`, `${ DATADIR }/19990102_fake.json.gz` ] );
		incompleteImportBatchCount = Number( incompleteImportBatchCount[ 0 ][ 'COUNT(*)' ] );
		assert.equal( incompleteImportBatchCount, 5 );
		assert.equal( await ipCount( '99.99.99.99' ), 0 );

		// Attempt to import a feed from a new date
		await exec( './main.sh --today 19990103_fake --yesterday 19990102_fake --debug true --batchsize 1' );

		// Assert the new feed wasn't imported and that instead the incomplete import was finished
		let rerunImportBatchCount = await connection.query( `
			SELECT COUNT(*)
			FROM import_status
			WHERE feed_file_yesterday = ?
			AND feed_file_today = ?
		`, [ `${ DATADIR }/19990101_fake.json.gz`, `${ DATADIR }/19990102_fake.json.gz` ] );
		rerunImportBatchCount = Number( rerunImportBatchCount[ 0 ][ 'COUNT(*)' ] );
		assert.equal( rerunImportBatchCount, 6 );
		assert.equal( await ipCount( '99.99.99.99' ), 1 );

		// Newest update shouldn't have run, expect 0 batches from import
		let newImportBatchCount = await connection.query( `
			SELECT COUNT(*)
			FROM import_status
			WHERE feed_file_yesterday = ?
			AND feed_file_today = ?
		`, [ `${ DATADIR }/19990102_fake.json.gz`, `${ DATADIR }/19990103_fake.json.gz` ] );
		newImportBatchCount = Number( newImportBatchCount[ 0 ][ 'COUNT(*)' ] );
		assert.equal( newImportBatchCount, 0 );
		assert.equal( await ipCount( '25.26.27.28' ), 0 );
		assert.equal( await ipCount( '99.99.99.99' ), 1 );

		// Re-attempt new import which should run now that the prior import has completed
		await exec( './main.sh --today 19990103_fake --yesterday 19990102_fake --debug true --batchsize 1' );
		let finalImportBatchCount = await connection.query( `
			SELECT COUNT(*)
			FROM import_status
			WHERE feed_file_yesterday = ?
			AND feed_file_today = ?
		`, [ `${ DATADIR }/19990102_fake.json.gz`, `${ DATADIR }/19990103_fake.json.gz` ] );
		finalImportBatchCount = Number( finalImportBatchCount[ 0 ][ 'COUNT(*)' ] );
		assert.equal( finalImportBatchCount, 6 );
		assert.equal( await ipCount( '25.26.27.28' ), 1 );
		assert.equal( await ipCount( '99.99.99.99' ), 0 );

		connection.end();
	} );

	it( 'should retry errors from a previous import', async () => {
		const connection = await mariadb.createConnection( options );

		// Import with an error
		await exec( './main.sh --today 19990102_fake_error --yesterday 19990101_fake --debug true --batchsize 1' );
		const importStatusResult = await connection.query( `
			SELECT batch_status
			FROM import_status
			WHERE feed_file_yesterday = ?
			AND feed_file_today = ?;
		`, [ `${ DATADIR }/19990101_fake.json.gz`, `${ DATADIR }/19990102_fake_error.json.gz` ] );
		const batchResultCount = {};
		batchResultCount[ jobStates.COMPLETE ] = 0;
		batchResultCount[ jobStates.ERROR ] = 0;
		importStatusResult.forEach( function ( batch ) {
			batchResultCount[ batch.batch_status ]++;
		} );

		// Assert that all batches attempted to run and that one had an error
		assert.equal( batchResultCount[ jobStates.COMPLETE ], 5 );
		assert.equal( batchResultCount[ jobStates.ERROR ], 1 );
		assert.equal( await ipCount( '37.38.39.40' ), 0 );
		assert.equal( await ipCount( '89.90.91.92' ), 0 );

		// Rename the file used for the import, allowing for a retry with the valid file
		await connection.query( `
			UPDATE import_status
			SET feed_file_today = ?
			WHERE feed_file_today = ?;
		`, [ `${ DATADIR }/19990102_fake.json.gz`, `${ DATADIR }/19990102_fake_error.json.gz` ] );

		// Re-run the import
		await exec( './main.sh --today 19990102_fake --yesterday 19990101_fake --debug true --batchsize 1' );

		const retryImportStatusResult = await connection.query( `
			SELECT
				batch_file,
				batch_count
			FROM import_status
			WHERE feed_file_yesterday = ?
			AND feed_file_today = ?
			AND batch_status = ?
		`, [ `${ DATADIR }/19990101_fake.json.gz`, `${ DATADIR }/19990102_fake.json.gz`, jobStates.RETRIED ] );

		// Assert that only the batch with the error was retried
		assert.equal( retryImportStatusResult.length, 1 );
		assert.equal( retryImportStatusResult[ 0 ].batch_file.toString(), `${ DATADIR }/sub/query_split_aaaad.sql` );

		// Assert that the batch count for a retry represents the entire expected import
		assert.equal( retryImportStatusResult[ 0 ].batch_count, 6 );

		// Expect 10 batches total (3 from initial import, 6 from update, 1 from retry)
		let postRetryBatchRunCount = await connection.query( 'SELECT COUNT(*) FROM import_status;' );
		postRetryBatchRunCount = Number( postRetryBatchRunCount[ 0 ][ 'COUNT(*)' ] );
		assert.equal( postRetryBatchRunCount, 10 );

		assert.equal( await ipCount( '37.38.39.40' ), 0 );
		assert.equal( await ipCount( '89.90.91.92' ), 1 );

		connection.end();
	} );

	it( 'should be able to recover from duplicate ip entries', async () => {
		const connection = await mariadb.createConnection( options );

		// Manually insert an IP actor. This simulates drift in the db vs the feeds.
		await connection.query( `INSERT INTO actor_data (ip,org,client_count,types,conc_city,conc_state,conc_country,countries,location_country,risks) VALUES ('99.99.99.99','Organization 5',0,1,'','','',0,'US',1);
		` );

		// Run an update
		await exec( './main.sh --today 19990202 --yesterday 19990201 --debug true --batchsize 1' );
		let firstUpdateBatchCount = await connection.query( `
			SELECT COUNT(*)
			FROM import_status
			WHERE feed_file_yesterday = ?
			AND feed_file_today = ?
			AND batch_status = ?
		`, [ `${ DATADIR }/19990201.json.gz`, `${ DATADIR }/19990202.json.gz`, jobStates.COMPLETE ] );
		firstUpdateBatchCount = Number( firstUpdateBatchCount[ 0 ][ 'COUNT(*)' ] );
		assert.equal( firstUpdateBatchCount, 6 );
		assert.equal( await ipCount( '99.99.99.99' ), 1 );

		await connection.end();
	} );
} );
