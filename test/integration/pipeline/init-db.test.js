'use strict';

const initDb = require( '../../../init-db.js' );
const { assert, expect } = require( 'chai' );
const mariadb = require( 'mariadb' );

const options = {
	host: process.env.MYSQL_HOST,
	user: process.env.MYSQL_RW_USER,
	password: process.env.MYSQL_RW_PASS,
	database: process.env.MYSQL_DATABASE,
	port: process.env.MYSQL_PORT
};

describe( 'init-db', () => {
	it( 'should initialize a database with the correct tables', async () => {
		await initDb.init( true );
		const connection = await mariadb.createConnection( options );
		const tables = await connection.query( 'SHOW TABLES' );
		assert.deepEqual(
			tables.map( ( table ) => {
				return Object.values( table )[ 0 ];
			} ).sort(),
			[
				'actor_data',
				'actor_data_behaviors',
				'actor_data_proxies',
				'actor_data_tunnels',
				'behaviors',
				'import_status',
				'proxies',
				'tunnels',
				'update_log'
			]
		);
		await connection.end();
	} );

	it( 'should skip an update if already applied', async () => {
		const ipoidDbPool = mariadb.createPool( options );
		expect( await initDb.updateSchema( 'init', 'foo', 'bar', ipoidDbPool ) ).to.equal( false );
		await ipoidDbPool.end();
	} );

	it( 'should run an update if not already applied', async () => {
		await initDb.init( false, 'test', './test/integration/pipeline/schema/updates.json' );
		const connection = await mariadb.createConnection( options );
		const tables = await connection.query( 'SHOW TABLES' );
		assert.deepEqual(
			tables.map( ( table ) => {
				return Object.values( table )[ 0 ];
			} ).sort(),
			[
				'actor_data',
				'actor_data_behaviors',
				'actor_data_proxies',
				'actor_data_tunnels',
				'behaviors',
				'import_status',
				'proxies',
				'test',
				'tunnels',
				'update_log'
			]
		);
		await connection.end();
	} );
} );
