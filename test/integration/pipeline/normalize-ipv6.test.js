'use strict';

const DATADIR = process.env.DATADIR;
const { assert } = require( 'chai' );
const { promisify } = require( 'util' );
const exec = promisify( require( 'child_process' ).exec );
const mariadb = require( 'mariadb' );
const normalizeIpv6 = require( '../../../maintenance/normalize-ipv6.js' );

const options = {
	host: process.env.MYSQL_HOST,
	user: process.env.MYSQL_RW_USER,
	password: process.env.MYSQL_RW_PASS,
	database: process.env.MYSQL_DATABASE,
	port: process.env.MYSQL_PORT
};

describe( 'normalize-ipv6', () => {
	// Load the data
	before( async function () {
		const files = [
			'19990301.json'
		];
		await Promise.all(
			files.map( async ( file ) => {
				await exec( `cp ./test/data/${ file } ${ DATADIR }` );
				await exec( `gzip ${ DATADIR }/${ file } -f` );
			} )
		);
	} );

	it( 'should be able to normalize ipv6 addresses', async () => {
		const connection = await mariadb.createConnection( options );

		// Initial import
		await exec( './main.sh --init true --today 19990301 --debug true' );

		// Manually run normalize script, as there aren't any IPs in the database before the import
		await normalizeIpv6.init( 10, 1 );

		// Confirm that IPs weren't duplicated (Expect only `14dd:ff28:d475:0:3d45:b731:9231:8536`)
		let totalIpCount = await connection.query( 'SELECT COUNT(ip) FROM actor_data;' );
		totalIpCount = Number( totalIpCount[ 0 ][ 'COUNT(ip)' ] );
		assert.equal( totalIpCount, 4 );

		// Expect normalized IPs
		let normalizedIpCount = await connection.query(
			'SELECT COUNT(ip) FROM actor_data WHERE ip = ?;',
			[ '2e91:0:76ea:0:863d:1c62:0:0' ]
		);
		normalizedIpCount = Number( normalizedIpCount[ 0 ][ 'COUNT(ip)' ] );

		assert.equal( normalizedIpCount, 1 );
		let insertedIpCount = await connection.query(
			'SELECT COUNT(ip) FROM actor_data WHERE ip = ?',
			[ '2e91:0:76ea:00:863d:1c62:000:0000' ]
		);
		insertedIpCount = Number( insertedIpCount[ 0 ][ 'COUNT(ip)' ] );
		assert.equal( insertedIpCount, 0 );

		connection.end();
	} );
} );
