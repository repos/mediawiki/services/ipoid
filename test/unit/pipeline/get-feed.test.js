'use strict';

const DATADIR = process.env.DATADIR;
const getFeed = require( '../../../get-feed.js' );
const { expect } = require( 'chai' );
const sinon = require( 'sinon' );
const fs = require( 'fs' );
const axios = require( 'axios' );

describe( 'get-feed', () => {

	afterEach( function () {
		sinon.restore();
		delete process.env.HTTPS_PROXY;
	} );

	it( 'should abort if the file already exists', async () => {
		await fs.writeFileSync( `${ DATADIR }/20000101_fake.json.gz`, 'test' );
		expect( await getFeed.init( '20000101_fake' ) ).to.equal( false );
	} );

	it( 'should fail if not given a date, and the file doesn\'t exist', async () => {
		getFeed.init( '20000102_fake' ).catch( ( e ) => {
			expect( e.message ).to.equal( 'Invalid date passed. Date should be in yyyymmdd format.' );
		} );
	} );

	it( 'should use a proxy if configured', async () => {
		process.env.HTTPS_PROXY = 'http://proxy:1313';
		process.env.SPUR_API_KEY = 'bar';
		const stub = sinon.stub( axios, 'get' );
		await getFeed.fetch( 'https://foo' );
		const args = stub.getCall( 0 ).lastArg;
		sinon.assert.match( args.headers, { Token: 'bar' } );
		sinon.assert.match( args.proxy, false );
		sinon.assert.match( args.httpsAgent.proxy.href, 'http://proxy:1313/' );
	} );

	it( 'should not use a proxy if env variables are not present', async () => {
		const stub = sinon.stub( axios, 'get' );
		process.env.SPUR_API_KEY = 'bar';
		await getFeed.fetch( 'https://foo' );
		sinon.assert.calledWith( stub, 'https://foo', {
			headers: { Token: 'bar' }, proxy: false
		} );
	} );
} );
