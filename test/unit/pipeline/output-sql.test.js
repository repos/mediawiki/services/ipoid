'use strict';

const DATADIR = process.env.DATADIR;
const outputSql = require( '../../../output-sql.js' );
const { expect } = require( 'chai' );
const fs = require( 'fs' );

describe( 'output-sql', () => {

	before( () => {
		fs.mkdirSync( DATADIR, { recursive: true } );
	} );

	it( 'should output the correct SQL file in import mode', async () => {
		// Remove existing file to ensure we compare the output of this test run.
		fs.rmSync( `${ DATADIR }/insert.sql`, {
			force: true
		} );
		await outputSql( 'test/data/20000101_fake.json', 'import' );
		const actual = fs.readFileSync( `${ DATADIR }/insert.sql` ).toString();
		const expected = fs.readFileSync( 'test/data/import.statements.sql' ).toString();
		expect( expected ).to.equal( actual );
	} );

	it( 'should output the correct SQL file in diff mode', async () => {
		// Remove existing files to ensure we compare the output of this test run.
		fs.rmSync( `${ DATADIR }/remove.sql`, {
			force: true
		} );
		fs.rmSync( `${ DATADIR }/update.sql`, {
			force: true
		} );
		fs.rmSync( `${ DATADIR }/insert.sql`, {
			force: true
		} );

		// output-sql will modify the input file, so test on a disposable copy
		const inputFileCanonical = 'test/data/20000101_20000102_fake.unique.sorted.json';
		const inputFileDisposable = inputFileCanonical + '.copy';
		fs.writeFileSync(
			inputFileDisposable,
			fs.readFileSync( inputFileCanonical )
		);
		await outputSql( inputFileDisposable, 'diff' );
		fs.unlinkSync( inputFileDisposable );

		const actual = fs.readFileSync( `${ DATADIR }/remove.sql` ).toString() +
			fs.readFileSync( `${ DATADIR }/update.sql` ).toString() +
			fs.readFileSync( `${ DATADIR }/insert.sql` ).toString();
		const expected = fs.readFileSync( 'test/data/diff.statements.sql' ).toString();
		expect( expected ).to.equal( actual );
	} );
} );
