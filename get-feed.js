'use strict';

const axios = require( 'axios' );
const fs = require( 'fs' );
const { HttpsProxyAgent } = require( 'https-proxy-agent' );
const DATADIR = process.env.DATADIR;
const logger = require( './pipeline-logger.js' ).getPipelineLogger();

/**
 * Fetch a resource using a GET request.
 *
 * @param {string} url The URL to fetch.
 * @param {string} responseType What response type to set in Axios params, e.g. 'stream'
 *
 * @return {Promise<axios.AxiosResponse<any>>}
 */
async function fetch( url, responseType ) {
	const axiosParams = {
		headers: {
			Token: process.env.SPUR_API_KEY
		},
		// See https://github.com/axios/axios/issues/2072#issuecomment-567473812
		proxy: false
	};
	if ( responseType ) {
		axiosParams.responseType = responseType;
	}
	if ( process.env.HTTPS_PROXY ) {
		// Set the header (token, response type) for the proxy agent.
		const agent = new HttpsProxyAgent( process.env.HTTPS_PROXY, axiosParams );
		axiosParams.httpsAgent = agent;
	}
	return await axios.get( url, axiosParams );
}

/**
 * @param {string} date
 * @return {boolean} true if the file was downloaded, false if the file already exists.
 */
async function init( date ) {
	// No need to re-download if the file exists locally; return early
	if ( fs.existsSync( `${ DATADIR }/${ date }.json.gz` ) ) {
		logger.info( `Feed for ${ date } already exists, skipping attempt to download.` );
		return false;
	}

	// Do a quick validation on the parameter passed through and throw if it doesn't
	// Date should match yyyymmdd format
	const dateValidator = /^([1-2][0-9][0-9][0-9])(0[1-9]|1[0-2])(0[1-9]|[1-2][0-9]|3[0-1])$/;
	if ( !dateValidator.test( date ) ) {
		throw new Error( 'Invalid date passed. Date should be in yyyymmdd format.' );
	}

	// Check if the file exists on Spur's server
	let fileCheck;

	await fetch( 'https://feeds.spur.us/v2/anonymous-residential/' )
		.then( ( response ) => {
			fileCheck = response.data.split( '\n' ).indexOf( `anonymous-residential/${ date }/` );
		} )
		.catch( ( error ) => {
			logger.error( {
				err: 'File check failed',
				info: error
			} );

			throw new Error( 'File check failed' );
		} );

	// Return early if today's file doesn't exist yet, or if date is nonexistent
	if ( fileCheck === -1 ) {
		throw new Error( `File for ${ date } doesn't exist. Aborting.` );
	}

	// Pull file down and save it at $DATADIR/${yyyymmdd}.json.gz
	logger.info( `Feed for ${ date } exists, pulling now...` );

	return await fetch( `https://feeds.spur.us/v2/anonymous-residential/${ date }/feed.json.gz`, 'stream' )
		.then( async ( response ) => {
			const feedOutput = fs.createWriteStream( `${ DATADIR }/${ date }.json.gz` );
			feedOutput.on( 'close', function () {
				logger.info( `Feed downloaded to ${ DATADIR }/${ date }.json.gz` );
			} );
			response.data.pipe( feedOutput );
			return true;
		} )
		.catch( ( error ) => {
			logger.error( {
				err: error,
				date: date
			} );
			throw error;
		} );
}

module.exports = {
	init: init,
	fetch: fetch
};
