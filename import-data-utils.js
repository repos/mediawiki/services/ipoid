'use strict';

const actorTypes = {
	UNKNOWN: 0b00001,
	DESKTOP: 0b00010,
	HEADLESS: 0b00100,
	IOT: 0b01000,
	MOBILE: 0b10000
};

const riskTypes = {
	UNKNOWN: 0b000001,
	CALLBACK_PROXY: 0b000010,
	GEO_MISMATCH: 0b000100,
	LOGIN_BRUTEFORCE: 0b001000,
	TUNNEL: 0b010000,
	WEB_SCRAPING: 0b100000
};

const tunnelTypes = {
	UNKNOWN: 0,
	VPN: 1,
	PROXY: 2,
	REMOTE_DESKTOP: 3
};

/**
 * Validate all type values being passed through
 *
 * @param {Array} types - Array of all types associated with the actor
 * @return {string} comma-delineated string of validated types
 *
 */
function getActorTypes( types ) {
	// Only allow types known by the SET
	// Convert array into a comma-separated string of values
	// Otherwise return UNKNOWN if no types were passed
	if ( types.length ) {
		return types
			.filter( function ( type ) {
				return Object.keys( actorTypes ).includes( type );
			} )
			.reduce( function ( acc, curr ) {
				return acc | actorTypes[ curr ];
			}, 0 );
	}
	return actorTypes.UNKNOWN;
}

/**
 * Validate all risk values being passed through
 *
 * @param {Array} risks - Array of all risks associated with the actor
 * @return {string} comma-delineated string of validated risks
 *
 */
function getActorRisks( risks ) {
	// Only allow types known by the SET
	// Convert array into a comma-separated string of values
	// Otherwise return UNKNOWN if no risks were passed
	if ( risks.length ) {
		return risks
			.filter( function ( risk, i, array ) {
				return Object.keys( riskTypes ).includes( risk );
			} )
			.reduce( function ( acc, curr ) {
				return acc | riskTypes[ curr ];
			}, 0 );
	}
	return riskTypes.UNKNOWN;
}

/**
 * Validate all behaviors values being passed through
 *
 * @param {Array} behaviors - Array of all behaviors associated with the actor
 * @return {Array} Array of validated behaviors
 *
 */
function getActorBehaviors( behaviors ) {
	return behaviors.filter( function ( behavior ) {
		return typeof behavior === 'string' && behavior.length;
	} );
}

/**
 * Validate all proxies values being passed through
 *
 * @param {Array} proxies - Array of all proxies associated with the actor
 * @return {Array} Array of validated proxies
 *
 */
function getActorProxies( proxies ) {
	return proxies.filter( function ( proxy ) {
		return typeof proxy === 'string' && proxy.length;
	} );
}

/**
 * Get tunnels as a valid array of tunnel objects
 *
 * @param {Mixed} tunnels The raw tunnels property from the data file
 * @return {Array} An array of tunnels
 */
function getTunnels( tunnels ) {
	if ( !Array.isArray( tunnels ) ) {
		return [];
	}

	return tunnels
		.filter( function ( tunnel ) {
			return tunnel.constructor === Object && !!tunnel.operator;
		} )
		.map( function ( tunnel ) {
			if ( !Object.keys( tunnelTypes ).includes( tunnel.type ) ) {
				tunnel.type = 'UNKNOWN';
			}
			return {
				operator: tunnel.operator,
				type: tunnelTypes[ tunnel.type ],
				typeValue: tunnel.type,
				anonymous: tunnel.anonymous
			};
		} );
}

module.exports.getActorBehaviors = getActorBehaviors;
module.exports.getActorProxies = getActorProxies;
module.exports.getActorTypes = getActorTypes;
module.exports.getActorRisks = getActorRisks;
module.exports.getTunnels = getTunnels;
module.exports.actorTypes = actorTypes;
module.exports.riskTypes = riskTypes;
module.exports.tunnelTypes = tunnelTypes;
