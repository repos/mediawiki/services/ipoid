'use strict';

const fs = require( 'fs' );
const readline = require( 'readline' );
const { tunnelTypes, getTunnels, getActorBehaviors, getActorProxies } = require( './import-data-utils' );
const DATADIR = process.env.DATADIR;
const logger = require( './pipeline-logger.js' ).getPipelineLogger();

function init( inputFilePath ) {
	return new Promise( ( resolve ) => {
		const dataStream = readline.createInterface( {
			// Usually $DATADIR/today.json
			input: fs.createReadStream( inputFilePath ),
			crlfDelay: Infinity
		} );

		const behaviors = new Set();
		const proxies = new Set();
		const vpnTunnels = new Set();
		const proxyTunnels = new Set();
		const remoteDesktopTunnels = new Set();
		const unknownTunnels = new Set();

		dataStream.on( 'line', function ( line ) {
			const data = JSON.parse( line );
			if ( data.client && Array.isArray( data.client.behaviors ) ) {
				const dataBehaviors = getActorBehaviors( data.client.behaviors );
				dataBehaviors.forEach( function ( behavior ) {
					{
						behaviors.add( behavior );
					}
				} );
			}
			if ( data.client && Array.isArray( data.client.proxies ) ) {
				const dataProxies = getActorProxies( data.client.proxies );
				dataProxies.forEach( function ( proxy ) {
					proxies.add( proxy );
				} );
			}
			if ( data.tunnels && Array.isArray( data.tunnels ) ) {
				const dataTunnels = getTunnels( data.tunnels );
				dataTunnels.forEach( function ( tunnel ) {
					if ( tunnel.type === tunnelTypes.VPN ) {
						vpnTunnels.add( tunnel.operator );
					} else if ( tunnel.type === tunnelTypes.PROXY ) {
						proxyTunnels.add( tunnel.operator );
					} else if ( tunnel.type === tunnelTypes.REMOTE_DESKTOP ) {
						remoteDesktopTunnels.add( tunnel.operator );
					} else if ( !tunnel.type.length ) {
						unknownTunnels.add( tunnel.operator );
					} else {
						logger.error(
							{
								err: 'Unknown type encountered while processing tunnel',
								type: tunnel
							}
						);
					}
				} );
			}
		} );

		dataStream.on( 'close', () => {
			const properties = {};
			properties.behaviors = Array.from( behaviors );
			properties.proxies = Array.from( proxies );

			// Insert tunnels with both possible values of anonymous
			// and null. Null is for backwards compatibility and to ensure that
			// if a tunnel has no anonymous value, it can still be inserted.
			properties.tunnels = [];
			[
				[ tunnelTypes.VPN, vpnTunnels ],
				[ tunnelTypes.PROXY, proxyTunnels ],
				[ tunnelTypes.REMOTE_DESKTOP, remoteDesktopTunnels ],
				[ tunnelTypes.UNKNOWN, unknownTunnels ]
			].forEach( function ( tunnelArray ) {
				const tunnelType = tunnelArray[ 0 ];
				const tunnelOperators = tunnelArray[ 1 ];

				tunnelOperators.forEach( function ( operator ) {
					properties.tunnels.push( {
						operator: operator,
						type: tunnelType,
						anonymous: 1
					} );
					properties.tunnels.push( {
						operator: operator,
						type: tunnelType,
						anonymous: 0
					} );
					properties.tunnels.push( {
						operator: operator,
						type: tunnelType,
						anonymous: null
					} );
				} );
			} );

			fs.writeFileSync( `${ DATADIR }/properties.json`, JSON.stringify( properties, null, '\t' ) );
			resolve();
		} );
	} );
}

module.exports = init;
