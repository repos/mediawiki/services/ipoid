CREATE TABLE IF NOT EXISTS actor_data (
   pkid BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
   ip VARBINARY(128) NOT NULL UNIQUE,
   org VARBINARY(1280),
   client_count INT UNSIGNED,
   -- 0b00001 = 'UNKNOWN', 0b00010 = 'DESKTOP', 0b00100 = 'HEADLESS,
   -- 0b01000 = 'IOT', 0b10000 = 'MOBILE'
   types SMALLINT,
   conc_city VARBINARY(128),
   conc_state VARBINARY(128),
   conc_country VARBINARY(32),
   countries INT,
   location_country VARBINARY(32),
   -- 0b000001 = 'UNKNOWN', 0b000010 = 'CALLBACK_PROXY', 0b000100 = 'GEO_MISMATCH,
   -- 0b001000 = 'LOGIN_BRUTEFORCE', 0b010000 = 'TUNNEL', 0b100000 = 'WEB_SCRAPING'
   risks SMALLINT,
   last_updated INT DEFAULT UNIX_TIMESTAMP(),
   PRIMARY KEY (pkid)
);

CREATE TABLE IF NOT EXISTS behaviors (
   pkid BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
   behavior VARBINARY(64) UNIQUE,
   PRIMARY KEY (pkid)
);

CREATE TABLE IF NOT EXISTS actor_data_behaviors (
   actor_data_id BIGINT UNSIGNED NOT NULL,
   behavior_id BIGINT UNSIGNED NOT NULL,
   PRIMARY KEY(actor_data_id, behavior_id)
);

CREATE TABLE IF NOT EXISTS proxies (
   pkid BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
   proxy VARBINARY(128) UNIQUE,
   PRIMARY KEY (pkid)
);

CREATE TABLE IF NOT EXISTS actor_data_proxies (
   actor_data_id BIGINT UNSIGNED NOT NULL,
   proxy_id BIGINT UNSIGNED NOT NULL,
   PRIMARY KEY(actor_data_id, proxy_id)
);

CREATE INDEX actor_data_proxies_proxyid ON actor_data_proxies(proxy_id);

CREATE TABLE IF NOT EXISTS tunnels (
   pkid BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
   operator VARBINARY(256) UNIQUE,
   -- 0 = 'UNKNOWN', 1 = 'PROXY', 2 = 'VPN'
   type TINYINT,
   anonymous BOOLEAN,
   PRIMARY KEY (pkid)
);

CREATE INDEX tunnels_type_operator ON tunnels(type, operator);

CREATE TABLE IF NOT EXISTS actor_data_tunnels (
   actor_data_id BIGINT UNSIGNED NOT NULL,
   tunnel_id BIGINT UNSIGNED NOT NULL,
   PRIMARY KEY(actor_data_id, tunnel_id)
);

CREATE INDEX actor_data_tunnels_tunnelid ON actor_data_tunnels(tunnel_id);

CREATE TABLE IF NOT EXISTS import_status (
   batchid BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
   timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
   feed_file_yesterday VARBINARY(128),
   feed_file_today VARBINARY(128),
   batch_file VARBINARY(128),
   batch_count INT,
   -- 0 = 'STARTED', 1 = 'COMPLETE', 2 = 'ERROR', 3 = 'RETRIED'
   batch_status TINYINT,
   PRIMARY KEY (batchid)
);

CREATE TABLE IF NOT EXISTS update_log (
   timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
   update_name VARBINARY(56),
   PRIMARY KEY (update_name)
);
