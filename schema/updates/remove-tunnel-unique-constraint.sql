-- Drop UNIQUE constraint from operator column
ALTER TABLE tunnels
    DROP INDEX operator;

-- Add UNIQUE constraint for operator/anonymous combo
ALTER TABLE tunnels
	ADD CONSTRAINT tunnels_operator_anonymous UNIQUE( operator, anonymous );
