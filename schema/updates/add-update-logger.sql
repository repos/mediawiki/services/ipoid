CREATE TABLE IF NOT EXISTS update_log (
   timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
   update_name VARBINARY(56),
   PRIMARY KEY (update_name)
)
