'use strict';

/**
 * Central location for getting a logger for pipeline scripts
 */

const { ecsFormat } = require( '@elastic/ecs-winston-format' );
const winston = require( 'winston' );

const logger = winston.createLogger( {
	format: ecsFormat( { convertReqRes: true } ),
	transports: [
		new winston.transports.Console()
	]
} );

/**
 * Return a logger with the trace.id property set from the TRACE_ID environment variable
 *
 * @return {Logger}
 */
function getPipelineLogger() {
	return logger.child( { 'trace.id': process.env.TRACE_ID } );
}

module.exports = {
	getPipelineLogger: getPipelineLogger
};
