#!/usr/bin/env bash

mkdir -p "$DATADIR"
npm install

# Bootstrap DB users used by the app
node -e "require('./create-users.js')();"

./wait-for-it.sh db:"$MYSQL_PORT" --timeout=30 --strict -- node && npm run watch
