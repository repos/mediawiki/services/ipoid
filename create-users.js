'use strict';

const mariadb = require( 'mariadb' );
const logger = require( './pipeline-logger.js' ).getPipelineLogger();

// Pool bound to MYSQL_DATABASE
const dbPool = mariadb.createPool( {
	host: process.env.MYSQL_HOST,
	user: process.env.MYSQL_ROOT_USER,
	password: process.env.MYSQL_ROOT_PASSWORD,
	port: process.env.MYSQL_PORT
} );

/**
 * Create users
 *
 * @return {Promise<void>}
 */
async function addUsers() {
	const connection = await dbPool.getConnection();
	// Create READ WRITE user
	await connection.query(
		`CREATE USER IF NOT EXISTS '${ process.env.MYSQL_RW_USER }' IDENTIFIED BY '${ process.env.MYSQL_RW_PASS }'`
	);
	await connection.query(
		`GRANT
			ALTER, CREATE, DELETE, DROP, INDEX, INSERT, SELECT, UPDATE
			ON *.*
			TO '${ process.env.MYSQL_RW_USER }'`
	);

	// Create READ ONLY user
	await connection.query(
		`CREATE USER IF NOT EXISTS '${ process.env.MYSQL_RO_USER }' IDENTIFIED BY '${ process.env.MYSQL_RO_PASS }'`
	);
	await connection.query(
		`GRANT
			SELECT
			ON *.*
			TO '${ process.env.MYSQL_RO_USER }'`
	);

	await connection.query( 'FLUSH PRIVILEGES;' );
	await connection.end();
}

/**
 * Create users and close connections.
 *
 * @return {Promise<void>}
 */
async function init() {
	await addUsers();
	await dbPool.end();
	logger.info( 'Added users' );
}

module.exports = init;
