'use strict';

const ipaddr = require( 'ipaddr.js' );
const logger = require( '../pipeline-logger.js' ).getPipelineLogger();
const mariadb = require( 'mariadb' );
const { promisify } = require( 'util' );

const options = {
	host: process.env.MYSQL_HOST,
	user: process.env.MYSQL_RW_USER,
	password: process.env.MYSQL_RW_PASS,
	database: process.env.MYSQL_DATABASE,
	port: process.env.MYSQL_PORT
};

const sleep = promisify( setTimeout );

/**
 * Find all IPv6 IPs in the database and normalize them
 *
 * @param {number} rowsPerBatch Number of entries to process each batch
 * @param {number} sleepBetween Time in ms to sleep in between batches
 */
async function init( rowsPerBatch, sleepBetween ) {
	const connection = await mariadb.createConnection( options );

	// Get the total number of rows and the expected number of batches
	// Get a count of all IPv6 rows. This is as broad as possible to cover all
	// variations of valid IPv6 addresses.
	let totalRows = await connection.query( 'SELECT COUNT(ip) FROM actor_data WHERE ip LIKE \'%:%\';' );
	totalRows = Number( totalRows[ 0 ][ 'COUNT(ip)' ] );

	// Set the total number of batches expected to run
	const batchCountTotal = Math.ceil( totalRows / rowsPerBatch );

	// In batches, check every ip against the normalized ip. If they don't match, normalize it.
	for ( let i = 0; i < batchCountTotal; i++ ) {
		const ips = await connection.query(
			'SELECT ip FROM actor_data WHERE ip LIKE \'%:%\' LIMIT ? OFFSET ?',
			[ rowsPerBatch, i * rowsPerBatch ]
		);
		await Promise.all(
			ips.map( async ( ip ) => {
				try {
					const ipInDb = ip.ip.toString();
					const normalizedIp = ipaddr.parse( ipInDb ).toNormalizedString();
					if ( ipInDb !== normalizedIp ) {
						// Check that the normalized IP doesn't exist in the database
						// If it does, delete the un-normalized IP. This assumes that there
						// is a transitory migration period, where new IPs coming in will
						// use the expected normalized format while older data can be inconsistent
						// and therefore the normalized IP's data is more recent.
						let ipExists = await connection.query(
							'SELECT COUNT(ip) FROM actor_data WHERE ip = ?',
							[ normalizedIp ]
						);
						ipExists = Number( ipExists[ 0 ][ 'COUNT(ip)' ] );
						if ( ipExists > 0 ) {
							await connection.query(
								'DELETE a FROM actor_data_behaviors a INNER JOIN actor_data b on a.actor_data_id = b.pkid WHERE b.ip = ?;',
								[ ipInDb ]
							);
							await connection.query(
								'DELETE a FROM actor_data_proxies a INNER JOIN actor_data b on a.actor_data_id = b.pkid WHERE b.ip = ?;',
								[ ipInDb ]
							);
							await connection.query(
								'DELETE a FROM actor_data_tunnels a INNER JOIN actor_data b on a.actor_data_id = b.pkid WHERE b.ip = ?;',
								[ ipInDb ]
							);
							await connection.query(
								'DELETE FROM actor_data WHERE ip = ?;',
								[ ipInDb ]
							);
						} else {
							// No data is associated with the normalized IP,
							// update the IP with the normalized format
							await connection.query(
								'UPDATE actor_data SET ip = ? WHERE ip = ?',
								[ normalizedIp, ipInDb ]
							);
						}
					}
				} catch ( e ) {
					logger.error( e );
				}
			} )
		);

		// Sleep in between batches
		await sleep( sleepBetween );
	}

	await connection.end();
}

module.exports = {
	init: init
};
