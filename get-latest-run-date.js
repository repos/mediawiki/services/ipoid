'use strict';

const mariadb = require( 'mariadb' );

const pool = mariadb.createPool( {
	host: process.env.MYSQL_HOST,
	user: process.env.MYSQL_RW_USER,
	password: process.env.MYSQL_RW_PASS,
	database: process.env.MYSQL_DATABASE,
	port: process.env.MYSQL_PORT
} );

// Get the 'today' feed file name from latest successful job run
// and return the date in yyyymmdd format. This is intended for use
// with ./main.sh
async function init() {
	const connection = await pool.getConnection();
	let latestImport = await connection.query( `
		SELECT feed_file_today FROM import_status
		WHERE batch_status=1
		ORDER BY timestamp DESC LIMIT 1;
	` );
	latestImport = latestImport[ 0 ];
	await connection.end();
	await pool.end();
	if ( latestImport ) {
		const dateGroup = /([1-2][0-9][0-9][0-9])(0[1-9]|1[0-2])(0[1-9]|[1-2][0-9]|3[0-1])/g;
		let date = latestImport.feed_file_today.toString().match( dateGroup );
		date = date ? date[ 0 ] : null;
		if ( date ) {
			console.log( date );
		}
	}
}

init();
