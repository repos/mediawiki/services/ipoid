'use strict';

const byline = require( 'byline' );
const fs = require( 'fs' );
const mariadb = require( 'mariadb' );
const DATADIR = process.env.DATADIR;
const { jobStates } = require( './import-status-utils' );
const { Transform } = require( 'stream' );
const logger = require( './pipeline-logger.js' ).getPipelineLogger();

async function main( inputFilePath, resolve ) {
	const pool = mariadb.createPool( {
		host: process.env.MYSQL_HOST,
		user: process.env.MYSQL_RW_USER,
		password: process.env.MYSQL_RW_PASS,
		database: process.env.MYSQL_DATABASE,
		port: process.env.MYSQL_PORT
	} );
	const updateConnection = await pool.getConnection();
	const statusConnection = await pool.getConnection();
	let counter = 0;
	let fileClosed = false;
	let batchid;
	let poolClosing = false;
	let batchError = false;

	async function closePool() {
		if ( !poolClosing ) {
			poolClosing = true;
			await updateConnection.end();
			await statusConnection.end();
			await pool.end();
			resolve();
		}
	}

	// Ensure that the queries have all completed before closing the pool. This
	// is called each time the counter is decremented. To guard against the
	// counter reaching 0 while the file is still being read, also check that
	// the file is closed before closing the pool.
	async function maybeClosePool( counter, batchid ) {
		if ( fileClosed && counter === 0 ) {
			logger.info( 'All updates complete' );
			await updateConnection.commit();

			// Log that the batch finished
			await statusConnection.query(
				'UPDATE import_status SET batch_status = ? WHERE batchid = ?', [ jobStates.COMPLETE, batchid ]
			);
			await closePool();
		}
	}

	// Pull some job context for later logging
	// Job info comes in as key:value pairs separated by newline
	// Transform that into an object
	const jobinfo = {};
	fs.readFileSync( `${ DATADIR }/job.info`, 'utf8' )
		.split( '\n' )
		.forEach( function ( attribute ) {
			const keyAndValues = attribute.split( ':' );
			// Length check to ignore the ending newline
			if ( keyAndValues.length > 1 ) {
				jobinfo[ keyAndValues[ 0 ] ] = keyAndValues[ 1 ];
			}
		} );

	const runQuery = new Transform( {
		async transform( chunk, encoding, transformCallback ) {
			const statements = chunk.toString().split( '@@@@@' );
			// Final item is empty
			statements.pop();
			// Add the number of queries to run to the counter. As each completes,
			// the counter is decremented, and when the counter reaches 0, we can
			// close the pool.
			counter += statements.length;
			for ( let i = 0; i < statements.length; i++ ) {
				const statement = statements[ i ];
				// Break out of the loop if a query encounters an error. Only
				// useful if the loop is long enough to still be iterating over
				// statements when the first error is encountered.
				if ( batchError ) {
					break;
				}
				try {
					await updateConnection.query( statement );
					counter--;
					await maybeClosePool( counter, batchid );
				} catch ( e ) {
					if ( e.sqlMessage.includes( 'Duplicate entry' ) ) {
						// Get IP to delete
						const ipRegex = /Duplicate entry '(.+)' for key 'ip'/;
						let ip = e.sqlMessage.match( ipRegex );

						// Only try and delete/re-insert if an IP is found.
						// If none was found, fall through and throw normally.
						if ( ip && ip.length > 1 ) {
							ip = ip[ 1 ];
							e.message = 'Duplicate entry found for IP, attempting to delete and re-insert';
							logger.info( e );

							try {
								// Manually delete the IP from the db for re-insertion.
								// In updates, the IP and its metadata are expected to be deleted
								// so they can be re-inserted by the update. Both have to be
								// manually deleted here in order to avoid collisions on the
								// primary key (the ip).
								await updateConnection.query(
									'DELETE a FROM actor_data_behaviors a INNER JOIN actor_data b on a.actor_data_id = b.pkid WHERE b.ip = ?;',
									[ ip ]
								);
								await updateConnection.query(
									'DELETE a FROM actor_data_proxies a INNER JOIN actor_data b on a.actor_data_id = b.pkid WHERE b.ip = ?;',
									[ ip ]
								);
								await updateConnection.query(
									'DELETE a FROM actor_data_tunnels a INNER JOIN actor_data b on a.actor_data_id = b.pkid WHERE b.ip = ?;',
									[ ip ]
								);
								await updateConnection.query(
									'DELETE FROM actor_data WHERE ip = ?;',
									[ ip ]
								);

								// Manually re-insert IP
								await updateConnection.query( statement );

								counter--;
								await maybeClosePool( counter, batchid );
								continue;
							} catch ( e ) {
								logger.error( e );
								batchError = true;
								await updateConnection.rollback();
								await statusConnection.query(
									'UPDATE import_status SET batch_status = ? WHERE batchid = ?', [ jobStates.ERROR, batchid ]
								);
								await closePool();
							}
						}
					}
					logger.error( e );
					batchError = true;
					await updateConnection.rollback();
					await statusConnection.query(
						'UPDATE import_status SET batch_status = ? WHERE batchid = ?', [ jobStates.ERROR, batchid ]
					);
					await closePool();
				}
			}

			transformCallback( null );
		}
	} );

	if ( !fs.existsSync( inputFilePath ) ) {
		throw new Error( 'File not available at declared path' );
	}

	// Log that a batch is starting
	await statusConnection.query( `INSERT INTO import_status
		(feed_file_yesterday, feed_file_today, batch_file, batch_count, batch_status)
			VALUES (?, ?, ?, ?, ?)`,
	[
		jobinfo.feed_file_yesterday,
		jobinfo.feed_file_today,
		inputFilePath,
		jobinfo.batch_count,
		jobStates.STARTED
	]
	);
	batchid = await statusConnection.query( 'SELECT LAST_INSERT_ID()' );
	batchid = batchid[ 0 ][ 'LAST_INSERT_ID()' ];
	await updateConnection.beginTransaction();
	const datastream = byline.createStream(
		fs.createReadStream( inputFilePath )
	)
		.pipe( runQuery )
		.pipe( fs.createWriteStream( '/dev/null' ) );

	datastream.on( 'close', () => {
		fileClosed = true;
		maybeClosePool( counter, batchid );
	} );
}

function init( inputFilePath ) {
	return new Promise( ( resolve ) => {
		main( inputFilePath, resolve );
	} );
}

module.exports = init;
