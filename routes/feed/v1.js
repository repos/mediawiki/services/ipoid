'use strict';

const sUtil = require( '../../lib/util' );
const feedUtil = require( '../../lib/feed-util' );
const ipaddr = require( 'ipaddr.js' );

/**
 * The main router object
 */
const router = sUtil.router();

/**
 * GET /
 * Gets some basic info about this service
 */
router.get( '/', ( req, res ) => {
	// simple sync return
	res.json( {
		name: 'ip info feed',
		version: '1.0',
		description: 'get information on ips',
		home: '/feed/v1/'
	} );

} );

/**
 * GET /feed/v1/:ip
 *
 * Return information for a single IP address or multiple IP addresses if the request has
 * the ?usePrefixSearch query parameter.
 *
 * The prefix search query uses a LIKE with a wildcard for the suffix, with an upper limit
 * of 1,000 results.
 *
 * If a single match is found, just that object is returned.
 * Otherwise, all rows matching the prefix search are returned.
 *
 * @return {IpData[]}
 */
router.get( '/ip/:ip', async ( req, res ) => {
	const connection = await feedUtil.createReadableConnection();
	let normalizedIp;
	try {
		normalizedIp = ipaddr.parse( req.params.ip ).toNormalizedString();
	} catch ( e ) {
		return res.status( 400 ).json( `${ req.params.ip } is not an IP address` );
	}

	let query;
	if ( req.query.usePrefixSearch ) {
		query = connection.query( 'SELECT * FROM actor_data WHERE ip LIKE ? LIMIT 1000;', normalizedIp + '%' );
	} else {
		query = connection.query( 'SELECT * FROM actor_data WHERE ip = ? LIMIT 1;', normalizedIp );
	}
	return await query.then( async ( allResults ) => {
		if ( allResults.length ) {
			const processedData = await feedUtil.processActorResults( connection, allResults );
			for ( const ip in processedData ) {
				// Exclude pkid from response. It's not useful to callers, and BigInt can't
				// be serialized easily.
				delete processedData[ ip ].pkid;

				// If the `usePrefixSearch` wasn't used, ensure that the IP is as the user passed it
				// since the format of the IP the user passes through can differ from what is stored
				if ( !req.query.usePrefixSearch && req.params.ip !== ip ) {
					processedData[ req.params.ip ] = processedData[ ip ];
					processedData[ req.params.ip ].ip = req.params.ip;
					delete processedData[ ip ];
				}
			}
			connection.end();
			return res.json( processedData );
		}
		connection.end();
		return res.status( 404 ).json( `No data found for ${ req.params.ip }` );
	} );
} );

/**
 * GET feed/v1/proxy/:label
 * Retrieves the IP addresses associated with a specific proxy label.
 */
router.get( '/proxy/:label', async ( req, res ) => {
	const connection = await feedUtil.createReadableConnection();
	return await connection.query(
		`SELECT ad.ip FROM actor_data ad
		JOIN actor_data_proxies adp ON ad.pkid = adp.actor_data_id
		JOIN proxies p ON adp.proxy_id = p.pkid WHERE p.proxy = ?;`,
		req.params.label
	)
		.then( async ( results ) => {
			connection.end();
			if ( results.length ) {
				const ips = results.map( ( row ) => row.ip.toString() );
				return res.json( ips );
			}
			return res.status( 404 ).end( `No proxy data found for ${ req.params.label }` );
		} )
		.catch( ( error ) => {
			return res.status( 500 ).end( 'Internal Server Error' );
		} );
} );

/**
 * GET /feed/v1/proxies
 *  Retrieves all proxy data
 */
router.get( '/proxies', async ( req, res ) => {
	const connection = await feedUtil.createReadableConnection();
	return await connection.query(
		`SELECT p.proxy AS label, GROUP_CONCAT(ad.ip) AS ips
		FROM actor_data ad
		JOIN actor_data_proxies adp ON ad.pkid = adp.actor_data_id
		JOIN proxies p ON adp.proxy_id = p.pkid
		GROUP BY p.proxy;`
	)
		.then( async ( results ) => {
			connection.end();
			const proxyData = {};
			if ( Array.isArray( results ) ) {
				results.forEach( ( row ) => {
					proxyData[ row.label ] = row.ips.toString().split( ',' );
				} );
			}
			return res.json( proxyData );
		} )
		.catch( ( error ) => {
			return res.status( 500 ).end( 'Internal Server Error' );
		} );
} );

/**
 * GET feed/v1/vpn/:label
 * Retrieves the IP addresses associated with a specific VPN tunnel label
 */
router.get( '/vpn/:label', async ( req, res ) => {
	const connection = await feedUtil.createReadableConnection();
	return await connection.query(
		`SELECT ad.ip
		FROM actor_data ad
		JOIN actor_data_tunnels adt ON ad.pkid = adt.actor_data_id
		JOIN tunnels t ON adt.tunnel_id = t.pkid
		WHERE t.type = 1 AND t.operator = ?;`,
		req.params.label
	)
		.then( async ( results ) => {
			connection.end();
			if ( results.length ) {
				const ips = results.map( ( row ) => row.ip.toString() );
				return res.json( ips );
			}
			return res.status( 404 ).end( `No tunnel vpn data found for ${ req.params.label }` );
		} )
		.catch( ( error ) => {
			return res.status( 500 ).end( 'Internal Server Error' );
		} );
} );

/**
 * GET /feed/v1/vpns
 * Retrieves the IP addresses associated with VPN tunnels
 */
router.get( '/vpns', async ( req, res ) => {
	const connection = await feedUtil.createReadableConnection();
	return await connection.query(
		`SELECT t.operator AS label, GROUP_CONCAT(ad.ip) AS ips
		FROM actor_data ad
		JOIN actor_data_tunnels adt ON ad.pkid = adt.actor_data_id
		JOIN tunnels t ON adt.tunnel_id = t.pkid
		WHERE t.type = 1
		GROUP BY t.operator;`
	)
		.then( async ( results ) => {
			connection.end();
			const proxyData = {};
			if ( Array.isArray( results ) ) {
				results.forEach( ( row ) => {
					proxyData[ row.label ] = row.ips.toString().split( ',' );
				} );
			}
			return res.json( proxyData );
		} )
		.catch( ( error ) => {
			return res.status( 500 ).end( 'Internal Server Error' );
		} );
} );

module.exports = () => {
	return {
		path: '/feed/v1',
		skip_domain: true,
		router
	};

};
